﻿using NUnit.Framework;
using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using ProfileManagement.Repository;
using ProfileManagement.StaticData;
using ProfileManagementTests;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfileManagement.Repository.Tests
{
    [TestFixture()]
    public class EducationRepositoryTests
    {
        ApplicationUser user;
        [SetUp]
        public void SetUp()
        {
            
        }

        [Test()]
        public void GetEducationWithRelationsTest()
        {
            var context = TestUtility.GetInMemoryAppDbContext();
            IEducationRepository educationRepository = new EducationRepository(context);
            IUserRepository userRepository = new UserRepository(context);
            ApplicationUser user = new ApplicationUser
            {
                DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "saiful@thekowcompany.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 1),
                UserName = "saif"
            };
            userRepository.Add(user).Wait();
            userRepository.Save().Wait();
            EducationInfo educationInfo = new EducationInfo
            {
                InstituteName = "AIUB",
                DegreeName = "B.Sc. in CSE",
                Session = "2011 ~ 2015",
                User = user
            };
            educationRepository.Add(educationInfo).Wait();
            educationRepository.Save().Wait();

            var education = educationRepository.GetEducationWithRelations(educationInfo.Id);
            Assert.IsNotNull(education.Result);
            Assert.IsNotNull(education.Result.User);
        }

        [Test()]
        public void GetEducationByIdTest()
        {
            var context = TestUtility.GetInMemoryAppDbContext();
            IEducationRepository educationRepository = new EducationRepository(context);
            IUserRepository userRepository = new UserRepository(context);
            ApplicationUser user = new ApplicationUser
            {
                DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "saiful@thekowcompany.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 1),
                UserName = "saif"
            };
            userRepository.Add(user).Wait();
            userRepository.Save().Wait();
            EducationInfo educationInfo = new EducationInfo
            {
                InstituteName = "AIUB",
                DegreeName = "B.Sc. in CSE",
                Session = "2011 ~ 2015",
                User = user
            };
            educationRepository.Add(educationInfo).Wait();
            educationRepository.Save().Wait();

            var education = educationRepository.GetById(educationInfo.Id);
            Assert.IsNotNull(education.Result);
            Assert.IsNotNull(education.Result.User);
        }
    }
}