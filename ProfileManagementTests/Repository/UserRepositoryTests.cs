﻿using NUnit.Framework;
using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using ProfileManagement.StaticData;
using ProfileManagementTests;
using System;
using System.Linq;

namespace ProfileManagement.Repository.Tests
{
    public class UserRepositoryTests
    {
        //[Test()]
        //public void CreateDatabaseTest()
        //{
        //    var context = TestUtility.GetAppDbContext();

        //    Assert.IsNotNull(context);
        //}

        [Test()]
        public void AddUserTest()
        {
            var context = TestUtility.GetInMemoryAppDbContext();
            ApplicationUser saiful = new ApplicationUser
            {
                DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "saiful@thekowcompany.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 1),
                UserName = "saif"
            };

            IUserRepository userRepository = new UserRepository(context);
            userRepository.Add(saiful).Wait();
            userRepository.Save().Wait();
            var userList = userRepository.GetAll();
            Assert.IsTrue(userList.Count() > 0);
        }
    }
}