﻿using Microsoft.EntityFrameworkCore;
using ProfileManagement.Data;

namespace ProfileManagementTests
{
    public class TestUtility
    {
        public static ApplicationDbContext GetAppDbContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlServer("Data Source=(local)\\SQLEXPRESS01;Initial Catalog=ProfileDB;User ID=sa;Password=1234")
                .Options;

            ApplicationDbContext context = new ApplicationDbContext(options);
            return context;
        }
        public static ApplicationDbContext GetInMemoryAppDbContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase("testdb")
                .Options;

            ApplicationDbContext context = new ApplicationDbContext(options);
            return context;
        }
    }
}
