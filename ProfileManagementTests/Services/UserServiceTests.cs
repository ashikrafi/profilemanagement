﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using ProfileManagement.Repository;
using ProfileManagement.Services;
using ProfileManagement.StaticData;
using ProfileManagementTests;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProfileManagement.Services.Tests
{
    public class UserServiceTests
    {
        [Test()]
        public void GetUserProfileTest()
        {
            var services = new ServiceCollection();
            services.AddAutoMapper(typeof(Startup));

            var serviceProvider = services.BuildServiceProvider();

            IMapper mapper = serviceProvider.GetService<IMapper>();

            var context = TestUtility.GetInMemoryAppDbContext();
            IUserRepository userRepository = new UserRepository(context);
            ApplicationUser saiful = new ApplicationUser
            {
                DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "saiful@thekowcompany.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 1),
                UserName = "saif",
                EducationInfos = new List<EducationInfo>
                {
                    new EducationInfo{ InstituteName = "AIUB", DegreeName = "B.Sc. in CSE", Session = "2011-2015" },
                    new EducationInfo{ InstituteName = "NSU", DegreeName = "B.Sc. in CSE", Session = "2010-2014" }
                },
                ExperienceInfos = new List<ExperienceInfo>
                {
                    new ExperienceInfo{ CompanyName = "CutOutWiz", Designation = "Sr. Software Engineer", Duration = "July 2020 ~ Present" },
                   new ExperienceInfo{ CompanyName = "Smart Technologies BD Ltd.", Designation = "Sr. Software Engineer", Duration = "September 2016 ~ June 2020" },
                   new ExperienceInfo{ CompanyName = "Proggasoft", Designation = "Software Engineer", Duration = "March 2015 ~ August 2016" }
                },
                //Skills = new List<Skill>
                //{
                //    new Skill{ Name = "Deep Learning", Percentage = 90, Color = "Green" },
                //    new Skill{ Name = "Natural Language Processing", Percentage = 50, Color = "Red" },
                //    new Skill{ Name = "Image Processing", Percentage = 60, Color = "Yellow" },
                //    new Skill{ Name = "Communication", Percentage = 30, Color = "Blue" },
                //    new Skill{ Name = "Writing", Percentage = 50, Color = "Orange"}
                //},
                //Activities = new List<UserActivity>
                //{
                //    new UserActivity{ Name = "Someone has given me a surprise", ActivityTime = new DateTime(2020, 09, 20) },
                //    new UserActivity{ Name = "My setting is updated", ActivityTime = new DateTime(2020, 09, 15) },
                //    new UserActivity{ Name = "Added new article", ActivityTime = new DateTime(2020, 09, 18) },
                //    new UserActivity{ Name = "Changed Profile Pic", ActivityTime = new DateTime(2020, 09, 1) }
                //},
                RecentContacts = new List<ApplicationUser>
                {
                    new ApplicationUser{ DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "ashikur@gmail.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 1),
                UserName = "Md Ashikur" },
                    new ApplicationUser{ DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "saiful@gmail.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 20),
                UserName = "Md Saiful" },
                    new ApplicationUser{ DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "anik@gmail.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 16),
                UserName = "Md Anik" },
                    new ApplicationUser{ DateOfBirth = new DateTime(1990, 1, 1),
                Designation = "",
                Email = "asif@gmail.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 12),
                UserName = "Md Asif" },
                    new ApplicationUser{ DateOfBirth = new DateTime(1988, 1, 1),
                Designation = "",
                Email = "mehedi@gmail.com",
                Gender = Gender.Male,
                MaritalStatus = MaritalStatus.Married,
                PhoneNumber = "01682006284",
                MemberSince = new DateTime(2020, 9, 20),
                UserName = "Md Mehedi" }
                }
            };

            userRepository.Add(saiful).Wait();
            userRepository.Save().Wait();
            //IUserService userService = new UserService(userRepository, mapper);
            //var profileViewModel = userService.GetUserProfile(saiful.Id);
            //Assert.IsNotNull(profileViewModel.Result);
            //Assert.IsNotNull(profileViewModel.Result.UserViewModel);
            //Assert.AreEqual(profileViewModel.Result.RecentContacts.Count, 5);
            //Assert.AreEqual(profileViewModel.Result.Skills.Count, 5);
            //Assert.AreEqual(profileViewModel.Result.Activities.Count, 4);
            //Assert.AreEqual(profileViewModel.Result.ExperienceInfos.Count, 3);
            //Assert.AreEqual(profileViewModel.Result.EducationInfos.Count, 2);
        }

        [Test()]
        public void GetDateOfBirthConvertionTest()
        {
            var services = new ServiceCollection();
            services.AddAutoMapper(typeof(Startup));

            var serviceProvider = services.BuildServiceProvider();

            IMapper mapper = serviceProvider.GetService<IMapper>();

            var context = TestUtility.GetAppDbContext();
            IUserRepository userRepository = new UserRepository(context);
            //ApplicationUser user = new ApplicationUser
            //{
            //    DateOfBirth = DateTime.Now,
            //    Designation = "",
            //    Email = "saiful@thekowcompany.com",
            //    Gender = Gender.Male,
            //    MaritalStatus = MaritalStatus.Married,
            //    PhoneNumber = "01682006284",
            //    MemberSince = new DateTime(2020, 9, 1),
            //    UserName = "saif",
            //};
            //userRepository.Add(user).Wait();
            //userRepository.Save().Wait();
            var user = userRepository.GetUserByEmail("ashik@gmail.com").Result;
            IUserService userService = new UserService(userRepository, mapper);
            var profileViewModel = userService.GetUserProfileEditViewModel(user.Id);
            Assert.IsNotNull(profileViewModel.Result);
            Assert.AreEqual("01/01/1990",profileViewModel.Result.DateOfBirth);
        }
    }
}