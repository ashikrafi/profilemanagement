﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProfileManagement.Migrations
{
    public partial class addedFullNameProperty_in_EmailData_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "EmailDatas",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "EmailDatas");
        }
    }
}
