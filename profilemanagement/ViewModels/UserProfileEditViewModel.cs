﻿using ProfileManagement.Models;
using ProfileManagement.StaticData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.ViewModels
{
    public class UserProfileEditViewModel
    {
        public string FullName { get; set; }
       
        public string FirstName { get; set; }
       
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }

        //[Required]
        public int SelectedGender { get; set; }
        public string DateOfBirth { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MMM dd, yyyy}", ApplyFormatInEditMode = false)]
        //public DateTime DateforTest { get; set; }

        // public DateTime DateOfBirthInDateFormat { get; set; }
        public int SelectedMaritalStatus { get; set; }

        //[Required]
        public string Designation { get; set; }

        

        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string Status { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string MemberSince { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public Gender Gender { get; set; }
        public string Interests { get; set; }
        public virtual List<EducationInfo> EducationInfos { get; set; }
        public string InstituteName { get; set; }
        public string DegreeName { get; set; }
        public string Session { get; set; }
        public virtual List<ExperienceInfo> ExperienceInfos { get; set; }
       
       // [Required]
        public string CompanyName { get; set; }
        public string Duration { get; set; }
        public string EmployeeDesignation { get; set; }

        public string UserId { get; set; }

    }
}
