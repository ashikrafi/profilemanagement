﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.ViewModels
{
    public class EmailViewModel
    {
        public string[] EmailTo { get; set; }
        public string[] UserIdTo { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }


        public string EmailDataId { get; set; }
        public string FromUserId { get; set; }
        public string ToUserId { get; set; }
        public string FromUserMail { get; set; }
        public string ToUserMail { get; set; }
        public string EmailSubject { get; set; }
        public string DateTime { get; set; }
        public bool Status { get; set; }
        public string[] FullNameArr { get; set; }

    }
}
