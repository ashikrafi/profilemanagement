﻿using ProfileManagement.Models;
using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.ViewModels
{
    public class ProfileViewModel
    {
        public UserViewModel UserViewModel { get; set; }
        public List<ExperienceInfo> ExperienceInfos { get; set; }
        public List<EducationInfo> EducationInfos { get; set; }
        public List<Skill> Skills { get; set; }
        public List<ApplicationUser> RecentContacts { get; set; }
        public List<UserActivity> Activities { get; set; }
    }
}
