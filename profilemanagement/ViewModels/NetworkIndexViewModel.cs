﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.ViewModels
{
    public class NetworkIndexViewModel
    {
        public List<ApplicationUser> MyNetworkUsers{ get; set; }

    }
}
