﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.ViewModels
{
    public class InboxViewModel
    {
        public IEnumerable<EmailData> inboxData { get; set; }
    }
}   
