﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Interfaces
{
    public interface IEmailDataRepository:IRepository<EmailData>
    {
        //Task<ApplicationUser> GetUserByEmail(string Email);
        IEnumerable<EmailData> GetInboxDataByUserId(string Id);

    }
}
