﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Interfaces
{
    public interface IEmailSend
    {
        bool SendMailGun(string subject, string emailBody, List<string> recievers, string type);
        //public static bool sendOrNot { get; set; }
    }
}
