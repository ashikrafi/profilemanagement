﻿using ProfileManagement.Models;
using ProfileManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Interfaces
{
    public interface IUserService
    {
        Task<bool> UpdateUser(ApplicationUser user);
        Task<ProfileViewModel> GetUserProfile(string UserId);
        Task<UserViewModel> GetUserViewModel(string UserId);
        Task<UserProfileEditViewModel> GetUserProfileEditViewModel(string UserId);

        //List<ApplicationUser> GetAllUsers();
    }
}
