﻿using ProfileManagement.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProfileManagement.Interfaces
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        Task<ApplicationUser> GetUserWithRelations(string Id);
        Task<ApplicationUser> GetUserByEmail(string Email);
        IEnumerable<ApplicationUser> GetAllWithOutCurrentUser(string id);
    }
}
