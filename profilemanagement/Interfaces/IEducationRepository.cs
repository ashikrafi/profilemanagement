﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Interfaces
{
    public interface IEducationRepository : IRepository<EducationInfo>
    {
        Task<EducationInfo> GetEducationWithRelations(long Id);
        IEnumerable<EducationInfo> GetEducationInfoForSingleUser(string id);
        public void UpdateEducation(EducationInfo entity);
    }
}
