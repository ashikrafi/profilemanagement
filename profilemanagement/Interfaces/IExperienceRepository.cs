﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Interfaces
{
    public interface IExperienceRepository : IRepository<ExperienceInfo>
    {
        IEnumerable<ExperienceInfo> GetExperienceInfoForSingleUser(string id);
        public void UpdateExperience(ExperienceInfo entity);
    }
}
