﻿using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProfileManagement.Data;

namespace ProfileManagement.Repository
{
    public class ExperienceRepository : BaseRepository<ExperienceInfo>, IExperienceRepository
    {
        private readonly ApplicationDbContext _context;
        public ExperienceRepository(ApplicationDbContext context) : base(context) { _context = context; }

        public async Task<ExperienceInfo> GetExperienceWithRelations(int Id)
        {
            try
            {
                var experience = await _context.ExperienceInfos.Include(x => x.User)
                     .FirstOrDefaultAsync(f => f.Id == Id);
                return experience;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public IEnumerable<ExperienceInfo> GetExperienceInfoForSingleUser(string id)
        {
            try
            {
                var experience = _context.ExperienceInfos.Where(x => x.User.Id == id).ToList();

                return experience;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void UpdateExperience(ExperienceInfo entity)
        {
            var entry = _context.ExperienceInfos.First(e => e.Id == entity.Id);
            _context.Entry(entry).CurrentValues.SetValues(entity);
            _context.SaveChanges();
           // return true;
        }
    }
}
