﻿using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProfileManagement.Data;

namespace ProfileManagement.Repository
{
    public class SkillRepository : BaseRepository<Skill>, ISkillRepository
    {
        private readonly ApplicationDbContext _context;
        public SkillRepository(ApplicationDbContext context) : base(context) { _context = context; }

        public async Task<Skill> GetSkillsWithRelations(int Id)
        {
            try
            {
                var skill = await _context.Skills.Include(x => x.User)
                                                 .FirstOrDefaultAsync(f => f.Id == Id);
                return skill;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
