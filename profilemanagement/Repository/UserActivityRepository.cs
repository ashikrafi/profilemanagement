﻿using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProfileManagement.Data;

namespace ProfileManagement.Repository
{
    public class UserActivityRepository : BaseRepository<UserActivity>, IUserActivityRepository
    {
        private readonly ApplicationDbContext _context;
        public UserActivityRepository(ApplicationDbContext context) : base(context) { _context = context; }

        public async Task<UserActivity> GetUserActivityWithRelations(int Id)
        {
            try
            {
                var activity = await _context.Activities.Include(x => x.User)
                     .FirstOrDefaultAsync(f => f.Id == Id);
                return activity;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
