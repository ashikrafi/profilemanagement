﻿using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProfileManagement.Data;

namespace ProfileManagement.Repository
{
    public class UserRepository : BaseRepository<ApplicationUser>, IUserRepository
    {
        private readonly ApplicationDbContext _context;
        public UserRepository(ApplicationDbContext context) : base(context) { _context = context; }

        public async Task<ApplicationUser> GetUserByEmail(string Email)
        {
            try
            {
                var user = await _context.Users.Include(x => x.ExperienceInfos)
                     .Include(x => x.EducationInfos)
                     .Include(x => x.RecentContacts)
                     // .Include(x => x.Skills)
                     .FirstOrDefaultAsync(f => f.Email == Email);
                return user;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<ApplicationUser> GetUserWithRelations(string Id)
        {
            try
            {
                var user = await _context.Users.Include(x => x.ExperienceInfos)
                     .Include(x => x.EducationInfos)
                     .Include(x => x.RecentContacts)
                    // .Include(x => x.Skills)
                     .FirstOrDefaultAsync(f=>f.Id == Id);
                return user;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    
        public IEnumerable<ApplicationUser> GetAllWithOutCurrentUser(string id)
        {
            var userList = _context.Users.Where(x => x.Id != id).ToList();
            return userList;
        }
    }
}
