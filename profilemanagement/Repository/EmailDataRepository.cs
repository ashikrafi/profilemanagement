﻿using ProfileManagement.Data;
using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Repository
{
    public class EmailDataRepository : BaseRepository<EmailData>, IEmailDataRepository
    {
        private readonly ApplicationDbContext _context;
        public EmailDataRepository(ApplicationDbContext context) : base(context) { _context = context; }
        public  IEnumerable<EmailData> GetInboxDataByUserId(string Id)
        {
            try
            {
                var emailData =  _context.EmailDatas.Where(x => x.FromUserId == Id).ToList();
                return emailData;
            }
            catch(Exception ex)
            {

                throw ex;
            }
        }
    }
}
