﻿using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProfileManagement.Data;

namespace ProfileManagement.Repository
{
    public class EducationRepository : BaseRepository<EducationInfo>,IEducationRepository
    {
        private readonly ApplicationDbContext _context;
        public EducationRepository(ApplicationDbContext context) : base(context) { _context = context; }

        public async Task<EducationInfo> GetEducationWithRelations(long Id)
        {
            try
            {
                var education = await _context.EducationInfos.Include(x => x.User)
                     .FirstOrDefaultAsync(f => f.Id == Id);
                return education;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<EducationInfo> GetEducationInfoForSingleUser(string id)
        {
            try
            {
                var education = _context.EducationInfos.Where(x => x.User.Id == id).ToList();
                    
                return education;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void UpdateEducation(EducationInfo entity)
        {
            var entry = _context.EducationInfos.First(e => e.Id == entity.Id);
            _context.Entry(entry).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }
    }
}
