﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.StaticData
{
    public static class StaticEmailData
    {
        public static string subject = "Intra-college Quiz Competition.";
        public static string emailBody = "<b>Hello Everyone!</b><br> This is to notify you all that an intra-college quiz competition is going to be conducted in our college on Jan 25 from 11:00 am in Auditorium – 01.<br>" +
            "Everyone is therefore invited to take part in the competition so that our department can win.<br>" +
            "For further inquiries, feel free to contact me<br>";

    }
}
