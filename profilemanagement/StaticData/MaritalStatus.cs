﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.StaticData
{
    public enum MaritalStatus
    {
        Single = 1,
        Married = 2
    }
}
