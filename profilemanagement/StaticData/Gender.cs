﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.StaticData
{
    public enum Gender
    {
        Male = 1,
        Female = 2,
        Others = 3
    }
}
