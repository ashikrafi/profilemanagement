﻿using Mailjet.Client;
using Mailjet.Client.Resources;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using ProfileManagement.Interfaces;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;



namespace ProfileManagement.Helpers
{
    public class EmailSender : IEmailSend
    {
        public string ReceverEmail { get; set; }
        public string BaseUrl { get; set; }
        public string Domain { get; set; }
        public string ApiKey { get; set; }
        public string ApiSecretKey { get; set; }
        public string EmailFrom { get; set; }
        //public static bool sendOrNot = false;

        private readonly IConfiguration _configuration;
        public EmailSender(IConfiguration configuration)
        {
            // ReceverEmail = receverEmail;
            _configuration = configuration;
            BaseUrl = _configuration.GetSection("AppSettings").GetSection("mailgun.baseurl").Value;
            Domain = _configuration.GetSection("AppSettings").GetSection("mailgun.domain").Value;
            ApiKey = _configuration.GetSection("AppSettings").GetSection("mailgun.apikey").Value;
            ApiSecretKey = _configuration.GetSection("AppSettings").GetSection("mailgun.apiscrtkey").Value;
            EmailFrom = _configuration.GetSection("AppSettings").GetSection("mailgun.emailsender").Value;
        }

        public bool SendEmai(string subject, string emailBody)
        {
            try
            {
                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.Credentials = new NetworkCredential(_configuration.GetSection("AppSettings").GetSection("smtp.email").Value, _configuration.GetSection("AppSettings").GetSection("smtp.emailPass").Value);
                smtp.EnableSsl = true;



                MailAddress from = new MailAddress(_configuration.GetSection("AppSettings").GetSection("smtp.email").Value, "CutOutWiz",
               System.Text.Encoding.UTF8);
                // Set destinations for the e-mail message.
                MailAddress to = new MailAddress(ReceverEmail);
                // Specify the message content.
                MailMessage message = new MailMessage(from, to);
                message.Body = emailBody;



                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = subject;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                smtp.Send(message);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool SendMailGun(string subject, string emailBody, List<string> recievers, string type)
        {
            try
            {
                var result = SendMailJetAsync(ApiKey, ApiSecretKey, EmailFrom, subject, emailBody, recievers, type);
                return Convert.ToBoolean(result.Result);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<bool> SendMailJetAsync(string ApiKey, string ApiSecretKey, string EmailFrom, string subject, string emailBody, List<string> recievers, string type)
        {
            emailBody = Regex.Replace(emailBody, @"\r\n?|\n", "<br />");
            var receiver = new List<JObject>();
            foreach (var address in recievers)
            {
                receiver.Add(new JObject {
                   {"Email", address.Trim()}
                   });
            }

            MailjetClient client = new MailjetClient(ApiKey, ApiSecretKey)
            {
                Version = ApiVersion.V3_1,
            };
            MailjetRequest request = new MailjetRequest
            {
                Resource = Send.Resource,
            }
               .Property(Send.Messages, new JArray {
                new JObject {
                 {"From", new JObject {
                  {"Email", EmailFrom},
                  }},

                 {"To", new JArray {
                  receiver
                  }},

                 {"Subject", subject},

                 {"HTMLPart", emailBody}
                 }
                });

            MailjetResponse response = await client.PostAsync(request);
            return response.IsSuccessStatusCode;
        }
    }
}