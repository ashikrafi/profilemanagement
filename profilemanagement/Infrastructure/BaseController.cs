﻿using Microsoft.AspNetCore.Mvc;

namespace cutoutwiz_automation.Infrastructure
{
    [Route("[controller]/[action]", Name = "[controller]_[action]")]
    public abstract class BaseController : Controller
    {
    }
}
