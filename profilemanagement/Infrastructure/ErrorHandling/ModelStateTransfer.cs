﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace cutoutwiz_automation.Infrastructure.ErrorHandling
{
    public abstract class ModelStateTransfer : ActionFilterAttribute
    {
        protected const string Key = nameof(ModelStateTransfer);
    }
}