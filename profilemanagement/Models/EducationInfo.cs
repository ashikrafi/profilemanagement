﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Models
{
    public class EducationInfo
    {
        public long Id { get; set; }
        public string InstituteName { get; set; }
        public string Session { get; set; }
        public string DegreeName { get; set; }
        public ApplicationUser User { get; set; }
    }
}
