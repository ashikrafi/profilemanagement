﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Models
{
    public class RecentContact
    {
        public string UserId1 { get; set; }
        public string UserId2 { get; set; }
    }
}
