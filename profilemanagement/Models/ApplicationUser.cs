﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using ProfileManagement.StaticData;

namespace ProfileManagement.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public long InstallationId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyWebsite { get; set; }
        public string OtherEmail { get; set; }
        public string OfficeAddress { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string OthersMemberEmail { get; set; }
        public string ProfilePicture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; internal set; }
        public virtual ICollection<EducationInfo> EducationInfos { get; set; }
        public virtual ICollection<ExperienceInfo> ExperienceInfos { get; set; }
        public ICollection<ApplicationUser> RecentContacts { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Designation { get; set; }
        public DateTime MemberSince { get; set; }
        public string Website { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Interests { get; set; }
        public string About { get; set; }
        public string Status { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
