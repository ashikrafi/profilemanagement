﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Models
{
    public class EmailData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public  string EmailDataId { get; set; }
        public string FromUserId { get; set; }
        public string ToUserId { get; set; }
        public string FromUserMail { get; set; }
        public string ToUserMail { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string  DateTime { get; set; }
        public bool Status { get; set; }
        public string FullName { get; set; }

        //public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
