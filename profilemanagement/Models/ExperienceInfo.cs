﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Models
{
    public class ExperienceInfo
    {
        public long Id { get; set; }
        public string CompanyName { get; set; }
        public string Designation { get; set; }
        public string Duration { get; set; }
        public ApplicationUser User { get; set; }
    }
}
