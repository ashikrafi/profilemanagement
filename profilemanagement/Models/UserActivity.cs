﻿using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Models
{
    public class UserActivity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime ActivityTime { get; set; }
        public ApplicationUser User { get; set; }
    }
}
