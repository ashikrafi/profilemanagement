﻿using AutoMapper;
using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using ProfileManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<ProfileViewModel> GetUserProfile(string UserId)
        {
            try
            {
                var user = await _userRepository.GetUserWithRelations(UserId);
                var userViewModel = _mapper.Map<ApplicationUser, UserViewModel>(user);
                var profileViewModel = new ProfileViewModel
                {
                    UserViewModel = userViewModel,
                    EducationInfos = user.EducationInfos.ToList(),
                    ExperienceInfos = user.ExperienceInfos.ToList(),
                    RecentContacts = user.RecentContacts.ToList(),
                    //Skills = user.Skills,
                    //Activities = user.Activities
                };
                return profileViewModel;
            }catch(Exception e)
            {
                throw e;
            }
        }

        public async Task<UserViewModel> GetUserViewModel(string UserId)
        {
            try
            {
                var user = await _userRepository.GetUserWithRelations(UserId);
                var userViewModel = _mapper.Map<ApplicationUser, UserViewModel>(user);

                return userViewModel;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<UserProfileEditViewModel> GetUserProfileEditViewModel(string UserId)
        {
            try
            {
                var user = await _userRepository.GetUserWithRelations(UserId);
                var userViewModel = _mapper.Map<ApplicationUser, UserProfileEditViewModel>(user);

                return userViewModel;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<bool> UpdateUser(ApplicationUser user)
        {
            try
            {
                _userRepository.Update(user);
                await _userRepository.Save();
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    
        //public List<ApplicationUser> GetAllUsers()
        //{
        //    return _userRepository.GetAll().ToList();
        //}
    }
}
