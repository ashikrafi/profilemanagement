﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProfileManagement.Data;
using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using ProfileManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Services
{
    public class EmailDataService : IEmailDataService
    {
        private readonly ApplicationDbContext _context;
        private readonly IEmailDataRepository _emailDataRepository;
        private readonly IMapper _mapper;
        public EmailDataService(ApplicationDbContext context, IEmailDataRepository emailDataRepository, IMapper mapper)
        {
            _context = context;
            _emailDataRepository = emailDataRepository;
            _mapper = mapper;
        }

        public IEnumerable<EmailData> GetInboxData(string userId)
        {
            try
            {
                //var InboxData =  _emailDataRepository.GetAll().Where(x=>x.FromUserId == userId).ToList();
                var InboxData = _context.EmailDatas.Where(x => x.ToUserId == userId).ToList();

                //var getEmailViewModel = new IEnumerable<EmailViewModel>();




                return InboxData;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<EmailData> GetOutboxData(string userId)
        {
            try
            {
                var OutboxData = _context.EmailDatas.Where(x => x.FromUserId == userId).ToList();

                return OutboxData;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        //public async Task<EmailDataViewModel> GetInboxData(string userId)
        //{
        //    try
        //    {
        //        return ok; 
        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public IEnumerable<ApplicationUser> GetAllUser()
        //{
        //    return _userRepository.GetAll().ToList();
        //}


        //public async Task<ApplicationUser> GetAllUser()
        //{

        //}

    }
}
