﻿using ProfileManagement.Models;
using ProfileManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Services
{
    public interface IEmailDataService
    {
        //Task<ApplicationUser> GetAllUser();
        //IEnumerable<ApplicationUser> GetAllUser();
        IEnumerable<EmailData> GetInboxData(string userId);
        IEnumerable<EmailData> GetOutboxData(string userId);

    }
}
