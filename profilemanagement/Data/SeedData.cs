﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProfileManagement.Models;
using ProfileManagement.StaticData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Data
{
    public static class SeedData<TIdentityUser, TIdentityRole>
         where TIdentityUser : ApplicationUser, new()
         where TIdentityRole : IdentityRole, new()
    {
        private const string DefaultAdminRoleName = Role.SuperAdmin;
        private const string DefaultUserRoleName = Role.User;
        private const string DefaultAdminUserEmail = "admin@gmail.com";
        private const string DefaultUserEmail = "ashik@gmail.com";
        private const string DefaultPassword = "123456aA@";

        private static readonly string[] DefaultRoles = { Role.SuperAdmin, Role.Admin, Role.User };

        private static async Task CreateDefaultRoles(RoleManager<TIdentityRole> roleManager)
        {
            foreach (string defaultRole in DefaultRoles)
            {
                // Make sure we have an Administrator role
                try
                {
                    if (!await roleManager.RoleExistsAsync(defaultRole))
                    {
                        var role = new TIdentityRole
                        {
                            Name = defaultRole
                        };

                        var roleResult = await roleManager.CreateAsync(role);
                        if (!roleResult.Succeeded)
                        {
                            throw new ApplicationException($"Could not create '{defaultRole}' role");
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private static async Task<TIdentityUser> CreateDefaultAdminUser(UserManager<TIdentityUser> userManager)
        {
            try
            {
                var user = await userManager.FindByEmailAsync(DefaultAdminUserEmail);
                if (user == null)
                {
                    user = new TIdentityUser
                    {
                        //UserNumber = 1,
                        UserName = "admin",
                        Email = DefaultAdminUserEmail,
                        EmailConfirmed = true,
                        Designation = "SuperAdmin",
                        FullName = "Super Admin",
                        Status = "Active",
                        Website = "https://www.google.com/",
                        Facebook = "https://www.facebook.com/",
                        Twitter = "https://www.twitter.com/",
                    };
                    var userResult = await userManager.CreateAsync(user, DefaultPassword);

                    if (!userResult.Succeeded)
                    {
                        throw new ApplicationException($"Could not create '{DefaultAdminUserEmail}' user");
                    }
                }
                return user;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private static async Task AddDefaultAdminRoleToDefaultAdminUser(
            UserManager<TIdentityUser> userManager,
            TIdentityUser user)
        {
            // Add user to Administrator role if it's not already associated
            if (!(await userManager.GetRolesAsync(user)).Contains(DefaultAdminRoleName))
            {
                var addToRoleResult = await userManager.AddToRoleAsync(user, DefaultAdminRoleName);
                if (!addToRoleResult.Succeeded)
                {
                    throw new ApplicationException(
                        $"Could not add user '{DefaultAdminUserEmail}' to '{DefaultAdminRoleName}' role");
                }
            }
        }

        public static async Task SeedDataAsync(IServiceProvider services)
        {
            var context = services.GetRequiredService<ApplicationDbContext>();
            var userManager = services.GetRequiredService<UserManager<TIdentityUser>>();
            var roleManager = services.GetRequiredService<RoleManager<TIdentityRole>>();

            //await context.Database.EnsureCreatedAsync(); //This causes migration error
            context.Database.Migrate();

            await CreateDefaultRoles(roleManager);
            var defaultAdminUser = await CreateDefaultAdminUser(userManager);
            await AddDefaultAdminRoleToDefaultAdminUser(userManager, defaultAdminUser);
            await SaveUserInfo(context, userManager);
        }

        private static async Task SaveUserInfo(ApplicationDbContext context, UserManager<TIdentityUser> userManager)
        {
            var user = await userManager.FindByEmailAsync(DefaultUserEmail);

            if (user == null)
            {
                var identityUser1 = new TIdentityUser
                {
                    UserName = DefaultUserEmail,
                    Email = DefaultUserEmail,
                    EmailConfirmed = true,
                    Designation = "Machine Learning Engineer",
                    FirstName = "Md. Ashikur",
                    LastName = "Rahman",
                    FullName = "Md. Ashikur Rahman",
                    DateOfBirth = new DateTime(1990, 1, 1),
                    Gender = Gender.Male,
                    MaritalStatus = MaritalStatus.Married,
                    PhoneNumber = "01675964080",
                    MemberSince = new DateTime(2020, 9, 1),
                    ProfilePicture = "/img/avatar-4.jpg",
                    Status = "Active",
                    Website = "https://www.google.com/",
                    Facebook = "https://www.facebook.com/",
                    Twitter = "https://www.twitter.com/",
                    Interests = "Deep Learning, Algorithms, Statistics & Probability",
                    About = "I know that I know nothing"
                };
                var defaultUser = await CreateDefaultUser(userManager, identityUser1);
                if (defaultUser == null) return;
                await AddDefaultUserRoleToDefaultUser(userManager, defaultUser);

                defaultUser.EducationInfos = new List<EducationInfo>
                {
                    new EducationInfo{ InstituteName = "AIUB", DegreeName = "B.Sc. in CSE", Session = "2011-2015" },
                    new EducationInfo{ InstituteName = "NSU", DegreeName = "B.Sc. in CSE", Session = "2010-2014" }
                };
                defaultUser.ExperienceInfos = new List<ExperienceInfo>
                {
                    new ExperienceInfo{ CompanyName = "CutOutWiz", Designation = "Sr. Software Engineer", Duration = "July 2020 ~ Present" },
                   new ExperienceInfo{ CompanyName = "Smart Technologies BD Ltd.", Designation = "Sr. Software Engineer", Duration = "September 2016 ~ June 2020" },
                   new ExperienceInfo{ CompanyName = "Proggasoft", Designation = "Software Engineer", Duration = "March 2015 ~ August 2016" }
                };
                //Skills = new List<Skill>
                //{
                //    new Skill{ Name = "Deep Learning", Percentage = 90, Color = "Green" },
                //    new Skill{ Name = "Natural Language Processing", Percentage = 50, Color = "Red" },
                //    new Skill{ Name = "Image Processing", Percentage = 60, Color = "Yellow" },
                //    new Skill{ Name = "Communication", Percentage = 30, Color = "Blue" },
                //    new Skill{ Name = "Writing", Percentage = 50, Color = "Orange"}
                //},
                //Activities = new List<UserActivity>
                //{
                //    new UserActivity{ Name = "Someone has given me a surprise", ActivityTime = new DateTime(2020, 09, 20) },
                //    new UserActivity{ Name = "My setting is updated", ActivityTime = new DateTime(2020, 09, 15) },
                //    new UserActivity{ Name = "Added new article", ActivityTime = new DateTime(2020, 09, 18) },
                //    new UserActivity{ Name = "Changed Profile Pic", ActivityTime = new DateTime(2020, 09, 1) }
                //},

                var mehedul = new TIdentityUser
                {
                    DateOfBirth = new DateTime(1990, 1, 1),
                    Designation = "Network Engineer",
                    Email = "mehedul@gmail.com",
                    Gender = Gender.Male,
                    MaritalStatus = MaritalStatus.Married,
                    PhoneNumber = "01682006284",
                    MemberSince = new DateTime(2020, 9, 1),
                    UserName = "mehedul@gmail.com",
                    FullName = "Md. Mehedul Islam",
                    FirstName = "Md. Mehedul",
                    LastName = "Islam",
                    ProfilePicture = "/img/avatar-5.jpg",
                    Status = "Active",
                    Website = "https://www.google.com/",
                    Facebook = "https://www.facebook.com/",
                    Twitter = "https://www.twitter.com/",
                    Interests = "Software Engineering",
                    About = "Hi"
                };
                mehedul = await CreateDefaultUser(userManager, mehedul);
                await AddDefaultUserRoleToDefaultUser(userManager, mehedul);

                var saiful = new TIdentityUser
                {
                    DateOfBirth = new DateTime(1990, 1, 1),
                    Designation = "Sr. Software Engineer",
                    Email = "saiful@gmail.com",
                    Gender = Gender.Male,
                    MaritalStatus = MaritalStatus.Married,
                    PhoneNumber = "01682006284",
                    MemberSince = new DateTime(2020, 9, 20),
                    UserName = "saiful@gmail.com",
                    FullName = "Md. Saiful Islam",
                    FirstName = "Md. Saiful",
                    LastName = "Islam",
                    ProfilePicture = "/img/avatar-3.jpg",
                    Status = "Active",
                    Website = "https://www.google.com/",
                    Facebook = "https://www.facebook.com/",
                    Twitter = "https://www.twitter.com/",
                    Interests = "Statistics & Probability",
                    About = "Nothing to say"
                };
                saiful = await CreateDefaultUser(userManager, saiful);
                await AddDefaultUserRoleToDefaultUser(userManager, saiful);

                var anik = new TIdentityUser
                {
                    DateOfBirth = new DateTime(1990, 1, 1),
                    Designation = "Product Owner",
                    Email = "anik@gmail.com",
                    Gender = Gender.Male,
                    MaritalStatus = MaritalStatus.Married,
                    PhoneNumber = "01682006284",
                    MemberSince = new DateTime(2020, 9, 16),
                    UserName = "anik@gmail.com",
                    FullName = "Md. Anik Ahmed",
                    FirstName = "Md. Anik",
                    LastName = "Ahmed",
                    ProfilePicture = "/img/avatar-5.jpg",
                    Status = "Active",
                    Website = "https://www.google.com/",
                    Facebook = "https://www.facebook.com/",
                    Twitter = "https://www.twitter.com/",
                    Interests = "Technologies",
                    About = "No more details"
                };
                anik = await CreateDefaultUser(userManager, anik);
                await AddDefaultUserRoleToDefaultUser(userManager, anik);

                var asif = new TIdentityUser
                {
                    DateOfBirth = new DateTime(1990, 1, 1),
                    Designation = "Product User",
                    Email = "asif@gmail.com",
                    Gender = Gender.Male,
                    MaritalStatus = MaritalStatus.Married,
                    PhoneNumber = "01682006284",
                    MemberSince = new DateTime(2020, 9, 12),
                    UserName = "asif@gmail.com",
                    FullName = "Md. Asif Khan",
                    FirstName = "Md. Asif",
                    LastName = "Khan",
                    ProfilePicture = "/img/avatar-3.jpg",
                    Status = "Inactive",
                    Website = "https://www.google.com/",
                    Facebook = "https://www.facebook.com/",
                    Twitter = "https://www.twitter.com/",
                    Interests = "Literature",
                    About = "Myself"
                };
                asif = await CreateDefaultUser(userManager, asif);
                await AddDefaultUserRoleToDefaultUser(userManager, asif);

                var mehedi = new TIdentityUser
                {
                    DateOfBirth = new DateTime(1988, 1, 1),
                    Designation = "Product User",
                    Email = "mehedi@gmail.com",
                    Gender = Gender.Male,
                    MaritalStatus = MaritalStatus.Married,
                    PhoneNumber = "01682006284",
                    MemberSince = new DateTime(2020, 9, 20),
                    UserName = "mehedi@gmail.com",
                    FullName = "Md. Mehedi Hasan",
                    FirstName = "Md. Mehedi",
                    LastName = "Hasan",
                    ProfilePicture = "/img/avatar-2.jpg",
                    Status = "Active",
                    Website = "https://www.google.com/",
                    Facebook = "https://www.facebook.com/",
                    Twitter = "https://www.twitter.com/",
                    Interests = "Reading",
                    About = "Interesting"
                };
                mehedi = await CreateDefaultUser(userManager, mehedi);
                await AddDefaultUserRoleToDefaultUser(userManager, mehedi);


                defaultUser.RecentContacts = new List<ApplicationUser>
                { mehedul, saiful, anik, asif, mehedi };
                context.SaveChanges();
            }

        }

        private static async Task<TIdentityUser> CreateDefaultUser(UserManager<TIdentityUser> userManager, TIdentityUser identityUser)
        {
            try
            {
                var user = await userManager.FindByEmailAsync(identityUser.Email);
                if (user == null)
                {
                    user = identityUser;
                    var userResult = await userManager.CreateAsync(user, DefaultPassword);

                    if (!userResult.Succeeded)
                    {
                        throw new ApplicationException($"Could not create '{DefaultUserEmail}' user");
                    }
                }
                return user;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private static async Task AddDefaultUserRoleToDefaultUser(
            UserManager<TIdentityUser> userManager,
            TIdentityUser user)
        {
            // Add user to Administrator role if it's not already associated
            if (!(await userManager.GetRolesAsync(user)).Contains(DefaultUserRoleName))
            {
                var addToRoleResult = await userManager.AddToRoleAsync(user, DefaultUserRoleName);
                if (!addToRoleResult.Succeeded)
                {
                    throw new ApplicationException(
                        $"Could not add user '{DefaultUserEmail}' to '{DefaultUserRoleName}' role");
                }
            }
        }
    }
}
