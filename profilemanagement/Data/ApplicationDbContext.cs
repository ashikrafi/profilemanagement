﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProfileManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileManagement.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        public DbSet<ExperienceInfo> ExperienceInfos { get; set; }
        public DbSet<EducationInfo> EducationInfos { get; set; }
        public DbSet<UserActivity> Activities { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<EmailData> EmailDatas { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>(user =>
            {
                user.HasIndex(u => u.UserName).IsUnique();
                //user.HasIndex(u => u.UserNumber).IsUnique();
                user.HasIndex(u => u.PhoneNumber).IsUnique();
            });

            //builder.Entity<ApplicationUser>()
            //    .HasMany<ExperienceInfo>(u => u.ExperienceInfos)
            //    .WithOne(i => i.User)
            //    .HasForeignKey(i => i.UserId)
            //    .OnDelete(DeleteBehavior.ClientSetNull);

            //builder.Entity<ApplicationUser>()
            //    .HasMany<EducationInfo>(u => u.EducationInfos)
            //    .WithOne(o => o.User)
            //    .HasForeignKey(o => o.UserId)
            //    .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}

