﻿using ProfileManagement.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProfileManagement
{
    public interface IUnitOfWork: IDisposable
    {
        //INotificationRepository Notifications { get; }
        IUserRepository Users { get; }
        //IOrderRepository Orders { get; }
        //ITeamRepository Teams { get; }
        //IJobCardRepository JobCards { get; }
        //IJobCardTaskRepository JobCardTasks { get; }
        //ILeaveApplicationRepository LeaveApplications { get; }
        //IEmployeeLeaveRepository EmployeeLeaves { get; }
        //IImageCategoryRepository ImageCategories { get; }
        //IProductionDataRepository ProductionDatas { get; }
        //IAttendanceRepository AttendanceRepository { get; }

        int Commit();
        Task<int> CommitAsync();
    }
}
