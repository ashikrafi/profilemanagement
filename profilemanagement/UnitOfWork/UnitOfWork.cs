﻿using ProfileManagement.Data;
using ProfileManagement.Interfaces;
using ProfileManagement.Repository;
using System;
using System.Threading.Tasks;

namespace ProfileManagement
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool isDisposed;
        private ApplicationDbContext _context;
        //private TeamRepository _teamRepository;
        private UserRepository _usersRepository;
        //private OrderRepository _ordersRepository;
        //private JobCardRepository _jobCardRepository;
        //private AttendanceRepository _attendanceRepository;
        //private JobCardTaskRepository _jobCardTaskRepository;
        //private NotificationRepository _notificationRepository;
        //private EmployeeLeaveRepository _employeeLeaveRepository;
        //private ImageCategoryRepository _imageCategoryRepository;
        //private ProductionDataRepository _productionDataRepository;
        //private LeaveApplicationRepository _leaveApplicationRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        //public INotificationRepository Notifications
        //{
        //    get
        //    {
        //        return _notificationRepository ??
        //            (_notificationRepository = new NotificationRepository(_context));
        //    }
        //}

        public IUserRepository Users
        {
            get
            {
                return _usersRepository ??
                    (_usersRepository = new UserRepository(_context));
            }
        }

        //public IOrderRepository Orders
        //{
        //    get
        //    {
        //        return _ordersRepository ??
        //            (_ordersRepository = new OrderRepository(_context));
        //    }
        //}

        //public ITeamRepository Teams
        //{
        //    get
        //    {
        //        return _teamRepository ?? 
        //            (_teamRepository = new TeamRepository(_context));
        //    }
        //}

        //public IJobCardRepository JobCards
        //{
        //    get
        //    {
        //        return _jobCardRepository ??
        //            (_jobCardRepository = new JobCardRepository(_context));
        //    }
        //}

        //public IJobCardTaskRepository JobCardTasks
        //{
        //    get
        //    {
        //        return _jobCardTaskRepository ??
        //            (_jobCardTaskRepository = new JobCardTaskRepository(_context));
        //    }
        //}

        //public ILeaveApplicationRepository LeaveApplications
        //{
        //    get
        //    {
        //        return _leaveApplicationRepository ??
        //            (_leaveApplicationRepository = new LeaveApplicationRepository(_context));
        //    }
        //}

        //public IEmployeeLeaveRepository EmployeeLeaves
        //{
        //    get
        //    {
        //        return _employeeLeaveRepository ??
        //            (_employeeLeaveRepository = new EmployeeLeaveRepository(_context));
        //    }
        //}

        //public IImageCategoryRepository ImageCategories
        //{
        //    get
        //    {
        //        return _imageCategoryRepository ??
        //            (_imageCategoryRepository = new ImageCategoryRepository(_context));
        //    }
        //}

        //public IProductionDataRepository ProductionDatas
        //{
        //    get
        //    {
        //        return _productionDataRepository ??
        //            (_productionDataRepository = new ProductionDataRepository(_context));
        //    }
        //}

        //public IAttendanceRepository AttendanceRepository
        //{
        //    get
        //    {
        //        return _attendanceRepository ??
        //            (_attendanceRepository = new AttendanceRepository(_context));
        //    }
        //}

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public Task<int> CommitAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed) return;

            if (disposing)
            {
                // free managed resources
                _context.Dispose();
            }

            isDisposed = true;
        }
    }
}
