﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProfileManagement.Data;
using ProfileManagement.Interfaces;
using ProfileManagement.Models;
using ProfileManagement.Services;
using ProfileManagement.ViewModels;

namespace ProfileManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        public readonly IUserService _userService;
        private readonly IUserRepository _userRepository;
        private readonly IConfiguration _configuration;
        private readonly IEmailSend _emailSend;
        private readonly IEmailDataService _emailDataService;
        private readonly IEmailDataRepository _emailDataRepository;
        public HomeController(UserManager<ApplicationUser> userManager, IUserService userService,
                             IUserRepository userRepository, IConfiguration configuration,
                             IEmailSend emailSend, ApplicationDbContext context, IEmailDataRepository emailDataRepository, IEmailDataService emailDataService)
        {
            _userManager = userManager;
            _userService = userService;
            _userRepository = userRepository;
            _configuration = configuration;
            _emailSend = emailSend;
            _context = context;
            _emailDataRepository = emailDataRepository;
            _emailDataService = emailDataService;
            //_emailDataService = emailDataService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult DynamicIndex()
        {
            var myNetworkViewMode = new NetworkIndexViewModel();

            var dataList = _userRepository.GetAll()
                .Include(x => x.EducationInfos)
                .Include(x => x.ExperienceInfos)
                .ToList();
            myNetworkViewMode.MyNetworkUsers = dataList;
            // myNetworkViewMode.MyNetworkUsers = _userService.GetAllUsers();
            return View(myNetworkViewMode);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<IActionResult> SendEmailInfo(EmailViewModel emailModelData)
        {
            var user = await _userManager.GetUserAsync(User);



            //if (user != null)
            //{
            // var ALLUser = _emailDataService.GetAllUser();

            // code for save email info
            var emailRecievers = emailModelData.EmailTo;
            var emailSubject = emailModelData.Subject;
            var emailBody = emailModelData.EmailBody;
            var personName = emailModelData.FullNameArr;

            //var isEmailSent = true;
            var isEmailSent = _emailSend.SendMailGun(emailSubject, emailBody, emailRecievers.ToList(), "html");
            var EmailList = new List<EmailData>();

            for (int i = 0; i < emailModelData.EmailTo.Length; i++)
            {
                var SingleEmailData = new EmailData()
                {
                    DateTime = DateTime.Now.ToString(),
                    EmailBody = emailModelData.EmailBody,
                    EmailSubject = emailModelData.Subject,
                    FromUserId = user.Id,
                    ToUserId = emailModelData.UserIdTo[i],
                    FromUserMail = user.Email.ToString(),
                    ToUserMail = emailModelData.EmailTo[i],
                    Status = isEmailSent,
                    FullName = personName[i]
                };
                EmailList.Add(SingleEmailData);
            }
            _emailDataRepository.AddMany(EmailList);
            await _emailDataRepository.Save();


            // var activeMembers = from allUser in ALLUser where emailRecievers.ToList().Select(x => x.).Contains(userList.emailID);
            //var activeMembers = from allUser in _context.Users where emailRecievers.ToList().Select(x =>x.).Contains(userList.emailID);


            //var isEmailSent = new Helpers.EmailSender(_configuration).SendMailGun(emailSubject, emailBody, emailRecievers.ToList(), "html");


            if (isEmailSent)
            {
                //var activeMembers = (from userList in db.tableName where receiverList.Select(x => x.emailID).Contains(userList.emailID));
                return Json(new { success = true, message = "save successfully" });
            }
            else
            {
                return Json(new { success = false, message = "sothing went worng" });
            }
            //}
            //string dd = DateTime.Now.Year.ToString();

            //return Json("success");
        }

        public async Task<IActionResult> MailInbox()
        {
            var user = await _userManager.GetUserAsync(User);
            var EmailInboxData = _emailDataService.GetInboxData(user.Id.ToString());

            //var date = from x in EmailInboxData
            //           select x.DateTime;

            //foreach (var item in EmailInboxData)
            //{
            //    var d = Convert.ToDateTime(item.DateTime).Date;
            //    var dd = Convert.ToDateTime(item.DateTime).Date.ToString("dd-MMM-yyyy");
            //    var ddd = dd.Trim();
            //}


            //var somthing = (from p in _context.EmailDatas
            //    join emp in _context.Users on p.ToUserId equals emp.Id
            //    where emp.Id 
            //    select new { p.EmpCode, p.PassportNumber, emp.EmpFullName })


            //var ToEmailUserName = _context.Users.ToList()
            //              .Where(h => _context.EmailDatas.ToList().Any(x => x.ToUserId == h.Id)).ToList();

            //var s = ToEmailUserName;
            //var ss = ToEmailUserName;

            //var inboxViewModelData = new InboxViewModel();
            //inboxViewModelData.inboxData = EmailInboxData;
            return View(EmailInboxData);
            //return View();
        }

        public async Task<IActionResult> MailOutbox()
        {
            var user = await _userManager.GetUserAsync(User);
            var EmailInboxData = _emailDataService.GetOutboxData(user.Id.ToString());

            return View(EmailInboxData);
        }
    }
}
