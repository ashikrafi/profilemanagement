﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProfileManagement.Models;
using ProfileManagement.Models.AccountViewModels;

using ProfileManagement.Data;
using Microsoft.AspNetCore.Cors;
using ProfileManagement.Services;
using System.Text.Encodings.Web;
using Microsoft.Extensions.Configuration;
using ProfileManagement.ViewModels;

namespace ProfileManagement.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [EnableCors("CORS")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;

        public AccountController(
            IConfiguration configuration,
            ApplicationDbContext context,
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _roleManager = roleManager;
            _context = context;
            _configuration = configuration;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var user = _context.Users.Where(x => x.UserName == model.Email).FirstOrDefault();
                if (user == null)
                {
                    return Json(new { status = false, message = "This email is not registered", type = "message" });
                }
                if (user != null && !user.EmailConfirmed)
                {
                    var userole = await _userManager.GetRolesAsync(user);
                    if (userole[0] == "Admin")
                    {
                        var res = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                        if (res.Succeeded)
                        {
                            return Json(new { status = true, message = "/AdminPanel/AdminFaq", type = "url" });
                        }
                    }
                    // need to add return
                   return Json(new { status = false, message = "Please verify your E-mail address.", type = "message" });
                }
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    //var userole = await _userManager.GetRolesAsync(user);
                    //if (userole[0] == "Admin")
                    //{
                    //    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                    //    return Json(new { status = true, message = "/AdminPanel/AdminFaq", type = "url" });
                    //}
                    return Json(new { status = true, message = "/User/Index", type = "url" });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid login attempt.", type = "message" });
                }
            }
            else
            {
                return Json(new { status = false, message = "Please enter valid credential.", type = "message" });
            }
        }
        public async Task<IActionResult> LoginFromApp(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    //return RedirectToAction(nameof(UploadController.UploadPhoto), "Upload");
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(AccountController.Login), "Account");
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(AccountController.Login), "Account");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return RedirectToAction(nameof(AccountController.Login), "Account");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> AdminRegister(RegisterViewModel model)
        {
            if (!await _roleManager.RoleExistsAsync("Admin"))
                await _roleManager.CreateAsync(new IdentityRole { Name = "Admin", NormalizedName = "ADMIN" });

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Email,
                    //FirstName = "Admin",
                    //LastName = "Panel"
                };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "Admin");
                    return Json(true);
                }
                else
                {
                    return Json(true);
                }
            }
            else
            {
                return Json(true);
            }

            return Json(true);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ThirdPartyPost(LoginViewModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                return Json("true");
            }
            else
            {
                return Json("false");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var model = new LoginWith2faViewModel { RememberMe = rememberMe };
            ViewData["ReturnUrl"] = returnUrl;

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var result = await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe, model.RememberMachine);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            else if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]

        public async Task<IActionResult> Register(CustomRegisterViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    FullName = $"{model.FirstName} {model.LastName}",
                    UserName = model.RegEmail,
                    Email = model.RegEmail,
                    //RegistrationDate = DateTime.UtcNow,
                    //InstallationId = _context.Users.Count() + 1
                    MemberSince = DateTime.Now,
                    Status = "Active"
                };
                var checkedUser = await _userManager.FindByEmailAsync(model.RegEmail);
                if (model.RegPassword == model.ConfirmPassword && checkedUser == null)
                {
                    var result = await _userManager.CreateAsync(user, model.RegPassword);
                    if (result.Succeeded)
                    {
                        if (!_context.Roles.Any(x => x.Name.Equals("Shop")))
                        {
                            await _roleManager.CreateAsync(new IdentityRole { Name = "Shop", NormalizedName = "SHOP" });
                        }
                        await _userManager.AddToRoleAsync(user, "Shop");
                        _logger.LogInformation("User created a new account with password.");
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                        var goBackUrl = Url.DefaultLink(user.Id, code, Request.Scheme);

                        await _emailSender.SendEmailConfirmationAsync(model.RegEmail, callbackUrl);
                        var body = $"Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>link</a>";
                        var htmlEmail = $"<html xmlns = 'http://www.w3.org/1999/xhtml'>" +
 "<head>" +
"<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>" +
"<meta name='viewport' content='width=device-width'/>" +
"</head>" +
"<body style='font-family:Lato;width:100%;height:100%;background:#f8f8f8;text-align:center;'>" +
"<div style='padding:10px 0 0 0;font-size: 100%;line-height:1.65;'>" +
    "<div style = 'display: block ; clear: both ; margin: auto; max-width: 580px ; width: 100%;" +
"border-style:solid;border-width:1px;border-color:#2ed3ae;background:white;'>" +
"<div style='padding:80px 0;background:#2ed3ae; color:white;'>" +
  "<h1 style ='margin:auto;font-size:32px;line-height:1.5;max-width:60%;text-transform:uppercase;'>Verify your email address</h1>" +
   "</div>" +
   "<div style='font-size:16px;font-weight:normal;margin-bottom:20px;'>" +
        "<h2 style='font-size:28px;line-height:1.25;'>Hi there,</h2>" +
             "<p>" +
                " You are just one click away from setting up your account with us. Please confirm your email address to get verified by clicking " +
    "on below button.</p>" +
 "<p>" +
     "<a style='display:inline-block;background: #2ed3ae;border:solid #2ed3ae;border-width:10px 80px 8px;" +
     $"font-weight:bold;border-radius:4px;color:#fdfdfd;text-decoration:none;'  href='{callbackUrl}'>Verify</a>" +
 "</p>" +
        "<a style='color:#2ed3ae;text-decoration:none;' href='http://www.cutoutwiz.com'> Profile Management </a>.</p>" +
"</div>" +
"<div style = 'margin-bottom:20px;'>" +
  "<a href='http://www.cutoutwiz.com'> <img style='width:20%;'src='~\\images\\Profile-logo.jpg'></a>" +
         "<p style='margin-bottom: 0;color:#888;font-size:14px;'>" +
              "<a style='color: #888;text-decoration:none;font-weight: bold;'href='mailto:'>info@cutoutwiz.com </a>" +
                     "<br>Copyright &copy; 2020.All rights reserved!" +
                   "</p>" +
               "</div>" +
           "</div>" +
         "</div>" +
   "</body>" +
   "</html>";
                        var isEmailSent = new Helpers.EmailSender(_configuration).SendMailGun("Confirm your email", htmlEmail, new List<string> { model.RegEmail }, "html");
                        //  await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created a new account with password.");
                        //return RedirectToAction(nameof(AccountController.Login),"Account");
                        var data = new
                        {
                            status = true,
                            msg = $"An activation email has been send to {model.RegEmail}."
                        };
                        return Json(data);
                    }
                    AddErrors(result);
                }
                else if (checkedUser != null)
                {
                    return Json(new
                    {
                        status = false,
                        msg = $"This email is already registered. Try to reset your password."
                    });
                }
                else if (model.RegPassword != model.ConfirmPassword)
                {
                    return Json(new
                    {
                        status = false,
                        msg = $"Password and confirm password didn't matched."
                    });
                }
            }
            return Json(new
            {
                status = false,
                msg = $"Failed to sign up,please try again."
            });
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(AccountController.Login), "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToAction(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }
            var firstName = info.Principal.Identity.Name.Split(' ')[0];
            var lastName = info.Principal.Identity.Name.Split(' ')[1];
            var isExists = _context.Users.Where(x => x.Email == email).Any();
            if (isExists)
            {
                var existingUser = _context.Users.Where(x => x.Email == email).FirstOrDefault();
                await _signInManager.SignInAsync(existingUser, isPersistent: false);
                return RedirectToAction(nameof(ProfileController.YourProfile), "Profile");
            }
            var user = new ApplicationUser
            {
                UserName = email,
                Email = email,
                //FullName = info.Principal.Identity.Name,
                //FirstName = firstName,
                //LastName = lastName,
                //InstallationId = _context.Users.Count() + 1
            };

            var result = await _userManager.CreateAsync(user);
            if (result.Succeeded)
            {
                if (!_context.Roles.Any(x => x.Name.Equals("Shop")))
                {
                    await _roleManager.CreateAsync(new IdentityRole { Name = "Shop", NormalizedName = "SHOP" });
                }
                await _userManager.AddToRoleAsync(user, "Shop");
                // result = await _userManager.AddLoginAsync(user, info);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction(nameof(ProfileController.Index), "Profile");
                    }
                    return RedirectToLocal(returnUrl);
                }
            }
            return RedirectToAction(nameof(AccountController.Login), "Account");
            //var user = _context.Users.Where(x=>x.Name==info.Principal.Identity.Name).FirstOrDefault();
            //// Sign in the user with this external login provider if the user already

            //var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            //if (result.Succeeded)
            //{


            //    _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);
            //    return RedirectToLocal(returnUrl);
            //}
            //if (result.IsLockedOut)
            //{
            //    return RedirectToAction(nameof(Lockout));
            //}
            //else
            //{
            //    // If the user does not have an account, then ask the user to create an account.
            //    ViewData["ReturnUrl"] = returnUrl;
            //    ViewData["LoginProvider"] = info.LoginProvider;
            //    var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            //    return View("ExternalLogin", new ExternalLoginViewModel { Email = email });
            //}
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    //FullName = info.Principal.Identity.Name
                };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    if (!_context.Roles.Any(x => x.Name.Equals("Shop")))
                    {
                        await _roleManager.CreateAsync(new IdentityRole { Name = "Shop", NormalizedName = "SHOP" });
                    }
                    await _userManager.AddToRoleAsync(user, "Shop");
                    // result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(nameof(ExternalLogin), model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(AccountController.Login), "Account");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return RedirectToAction(nameof(AccountController.Login), "Account");
            }
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetDefaultLink(string userId, string code)
        {
            return RedirectToAction(nameof(ProfileController.Index), "Profile");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    return Json("not");
                    // Don't reveal that the user does not exist or is not confirmed
                    // return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                   $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");

                var body = $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>";
                var htmlEmail = $"<html xmlns = 'http://www.w3.org/1999/xhtml'>" +
              "<head>" +
   "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>" +
        "<meta name='viewport' content='width=device-width'/>" +
       "</head>" +
       "<body style='font-family:Lato;width:100%;height:100%;background:#f8f8f8;text-align:center;'>" +
            "<div style='padding:10px 0 0 0;font-size: 100%;line-height:1.65;'>" +
                 "<div style = 'display: block ; clear: both ; margin: auto; max-width: 580px ; width: 100%;" +
      "border-style:solid;border-width:1px;border-color:#2ed3ae;background:white;'>" +
          "<div style='padding:80px 0;background:#2ed3ae; color:white;'>" +
               "<h1 style ='margin:auto;font-size:32px;line-height:1.5;max-width:60%;text-transform:uppercase;'>Reset your password</h1>" +
                "</div>" +
                "<div style='font-size:16px;font-weight:normal;margin-bottom:20px;'>" +
                     "<h2 style='font-size:28px;line-height:1.25;'>Hi there,</h2>" +
                          "<p>" +
                             "To reset your password, please click the below button." +
                 "on below button.</p>" +
              "<p>" +
                  "<a style='display:inline-block;background: #2ed3ae;border:solid #2ed3ae;border-width:10px 80px 8px;" +
                  $"font-weight:bold;border-radius:4px;color:#fdfdfd;text-decoration:none;'  href='{callbackUrl}'>Reset</a>" +
              "</p>" +
          "</div>" +
          "<div style = 'margin-bottom:20px;'>" +
               "<a href='http://www.cutoutwiz.com'> <img style='width:20%;'src='https://pbs.twimg.com/profile_images/915125227536785408/R2u0jTSs_400x400.jpg'/></a>" +
                      "<p style='margin-bottom: 0;color:#888;font-size:14px;'>" +
                           "<a style='color: #888;text-decoration:none;font-weight: bold;'href='mailto:'>info@cutoutwiz.com </a>" +
                                  "<br>Copyright &copy; 2020.All rights reserved!" +
                                "</p>" +
                            "</div>" +
                        "</div>" +
                      "</div>" +
                "</body>" +
                "</html>";
                var isEmailSent = new Helpers.EmailSender(_configuration).SendMailGun("Reset Password", htmlEmail, new List<string> { model.Email }, "html");
                return Json(isEmailSent);
                //return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }
            // If we got this far, something failed, redisplay form
            return Json(false);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]

        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(AccountController.Login), "Account");
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Index");
            }
        }

        #endregion
    }
}
