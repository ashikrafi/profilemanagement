﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ProfileManagement.Models;
using Microsoft.AspNetCore.Identity;
using ProfileManagement.Interfaces;
using ProfileManagement.ViewModels;
using ProfileManagement.StaticData;
using ProfileManagement.Helpers;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;

namespace ProfileManagement.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IUserService _userService;
        private readonly IUserRepository _userRepository;
        private string Root_Directory;
        private readonly IConfiguration _configuration;
        private readonly IEducationRepository _educationRepository;
        private readonly IExperienceRepository _experienceRepository;
        
        public UserController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IUserService userService, IConfiguration configuration, IEducationRepository educationRepository, IExperienceRepository experienceRepository, IUserRepository userRepository)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _userRepository = userRepository;
            _configuration = configuration;
            _educationRepository = educationRepository;
            _experienceRepository = experienceRepository;
            Root_Directory = _configuration.GetSection("AppSettings").GetSection("Root_Directory").Value;
        }

        public async Task<IActionResult> Index()
        {
            ProfileViewModel profileViewModel = new ProfileViewModel();
            var user = await _userManager.GetUserAsync(User);
            if (user != null)
            {
                profileViewModel = await _userService.GetUserProfile(user.Id);
            }
            ///// check if profile data is emplty or not if empty then redirect to edit action
            if (profileViewModel.UserViewModel.Interests == null || profileViewModel.UserViewModel.PhoneNumber == null || profileViewModel.ExperienceInfos.Count <= 0)
            {
                return RedirectToAction("EditUser");
            }

            return View(profileViewModel);
            //return View();
        }
        public async Task<IActionResult> EditUser()
        {
            var user = await _userManager.GetUserAsync(User);
            UserProfileEditViewModel userViewModel = new UserProfileEditViewModel();
            if (user != null)
            {
                userViewModel = await _userService.GetUserProfileEditViewModel(user.Id);
                //userViewModel.DateforTest = Convert.ToDateTime(userViewModel.DateOfBirth);
                userViewModel.SelectedGender = (int)user.Gender;
                userViewModel.SelectedMaritalStatus = (int)user.MaritalStatus;
                userViewModel.DateOfBirth = Convert.ToDateTime(userViewModel.DateOfBirth).Date.ToString("yyyy-MM-dd");
            }

            return View(userViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> SaveProfile(UserProfileEditViewModel profile)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    //if (!ModelState.IsValid)
                    //{
                    //    //ModelState.AddModelError(nameof(profile), "please give valid data");
                    //    //return Json(new { error = true, success = false, message = "please give valid data" });
                    //    return RedirectToAction("EditUser");
                    //    // return RedirectToAction("EditUser");
                    //}
                    if (string.IsNullOrWhiteSpace(profile.FirstName) || string.IsNullOrWhiteSpace(profile.Designation) 
                        || string.IsNullOrWhiteSpace(profile.LastName) || string.IsNullOrWhiteSpace(profile.Interests)
                        || profile.SelectedGender == 0 || profile.SelectedMaritalStatus == 0)
                        return Json(new { error = true, success = false, message = "please give valid data" });

                    user.FirstName = profile.FirstName;
                    user.LastName = profile.LastName;
                    user.Gender = (Gender)profile.SelectedGender;
                    user.MaritalStatus = (MaritalStatus)profile.SelectedMaritalStatus;
                    user.DateOfBirth = Convert.ToDateTime(profile.DateOfBirth);
                    user.FullName = profile.FirstName + " " + profile.LastName;
                    user.Designation = profile.Designation;
                    user.Country = profile.Country;
                    user.City = profile.City;
                    user.Interests = profile.Interests;

                    await _userService.UpdateUser(user);


                    //return View("EditUser");
                    return Json(new { success = true, message = "save successfully" });
                    //if (!_context.Users.Where(x => x.PhoneNumber == profile.Mobile && x.Email != user.Email).Any())
                    //{
                    //user.PhoneNumber = profile.PhoneNumber ?? "";
                    //user.CompanyName = profile.CompanyName ?? "";
                    //user.OfficeAddress = profile.OfficeAddress ?? "";
                    //user.CompanyWebsite = profile.CompanyWebsite ?? "";
                    //user.OthersMemberEmail = profile.OthersMemberEmail ?? "";

                    //_context.SaveChanges();
                    //if (!string.IsNullOrEmpty(profile.Password) && !string.IsNullOrEmpty(profile.NewPassword) && !string.IsNullOrEmpty(profile.ConfirmPassword) && profile.NewPassword.Equals(profile.ConfirmPassword))
                    //{
                    //    await _userManager.ChangePasswordAsync(user, profile.Password, profile.NewPassword);
                    //    await _signInManager.SignOutAsync();
                    //    return Json("pass");
                    //}

                    //return RedirectToAction("Index");
                    //}
                    //else
                    //{
                    //    return Json("Phone number already used.");
                    //}
                }
                else
                {
                    //return Json("error");
                    return Json(new { error = true, success = false, message = "No user detected" });
                    //return RedirectToAction("EditUser");
                }
            }
            catch (Exception ex)
            {
                //return Json("error");
                return Json(new { error = true, success = false, message = ex.Message });
                //return RedirectToAction("EditUser");
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveContactInfoWithAjax(UserProfileEditViewModel profile)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    if (String.IsNullOrEmpty(profile.PhoneNumber) || String.IsNullOrEmpty(profile.Email))
                    {
                        return Json(new { error = true, success = false, message = "please give valid data" });
                        // return RedirectToAction("EditUser");
                    }
                    profile.UserId = user.Id;
                    // check for duplicate value
                    //var contactInfo = _userRepository.GetAllWithOutCurrentUser(user.Id);
                    var isDuplicate = CheckUserDuplicateContactInfo(profile);
                    if (isDuplicate)
                    {
                        return Json(new { error = true, success = false, message = "Data already exists!." });
                    }


                    // check this
                    user.PhoneNumber = profile.PhoneNumber;
                    user.Email = profile.Email;
                    user.Website = profile.Website;
                    user.Facebook = profile.Facebook;
                    user.Twitter = profile.Twitter;
                    await _userService.UpdateUser(user);
                    //return RedirectToAction("Index");
                    //return Json("success");
                    return Json(new { success = true, message = "save successfully" });
                }
                else
                {
                    return Json(new { success = false, message = "somthing went wrong" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        //[HttpPost]
        //public async Task<IActionResult> SaveContactInfo(UserProfileEditViewModel profile)
        //{
        //    try
        //    {
        //        var user = await _userManager.GetUserAsync(User);
        //        if (user != null)
        //        {
        //            user.PhoneNumber = profile.PhoneNumber;
        //            user.Email = profile.Email;
        //            user.Website = profile.Website;
        //            user.Facebook = profile.Facebook;
        //            user.Twitter = profile.Twitter;
        //            await _userService.UpdateUser(user);
        //            return RedirectToAction("Index");
        //            //return Json("success");
        //        }
        //        else
        //        {
        //            return Json("error");
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Json("error");
        //    }
        //}

        [HttpPost]
        public async Task<IActionResult> SaveEducationInfo(UserProfileEditViewModel profile)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    if (profile.InstituteName != null && profile.DegreeName != null)
                    {
                        // check for duplicate value

                        var isDuplicate = CheckDuplicateEducationData(profile.InstituteName.ToLower(), profile.DegreeName.ToLower(), user);
                        if (isDuplicate)
                        {
                            return Json(new { error = true, success = false, message = "Data already exists!." });
                        }

                        EducationInfo educationInfo = new EducationInfo
                        {
                            InstituteName = profile.InstituteName,
                            DegreeName = profile.DegreeName,
                            Session = profile.Session,
                            User = user
                        };
                        await _educationRepository.Add(educationInfo);
                        await _educationRepository.Save();

                        //return RedirectToAction("Index");
                        return Json(new { success = true, message = "save successfully" });
                    }
                    else
                    {
                        return Json(new { error = true, success = false, message = "Not valid Data" });
                    }
                }
                else
                {
                    return Json(new { error = true, success = false, message = "No user detected" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = true, success = false, message = ex.Message });
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveExperienceInfo(UserProfileEditViewModel profile)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {

                    if (profile.EmployeeDesignation != null && profile.CompanyName != null)
                    {

                        // check for duplicate value

                        var isDuplicate = CheckDuplicateExperiencedData(profile.EmployeeDesignation.ToLower(), profile.CompanyName.ToLower(), user);
                        if (isDuplicate)
                        {
                            return Json(new { error = true, success = false, message = "Data already exists!." });
                        }

                        ExperienceInfo experienceInfo = new ExperienceInfo
                        {
                            CompanyName = profile.CompanyName,
                            Designation = profile.EmployeeDesignation,
                            Duration = profile.Duration,
                            User = user
                        };
                        await _experienceRepository.Add(experienceInfo);
                        await _experienceRepository.Save();

                        return Json(new { success = true, message = "save successfully" });
                    }

                    return Json(new { error = true, success = false, message = "No data for insertion" });
                }
                else
                {
                    return Json(new { error = true, success = false, message = "somthing went wrong. No User available" });

                }
            }
            catch (Exception)
            {

                return Json(new { error = true, success = false, message = "somthing went wrong" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveAccountInfo(UserProfileEditViewModel model)
        {
            var user = _userManager.GetUserAsync(User).Result;
            if (!string.IsNullOrEmpty(model.Password) && (model.NewPassword == model.ConfirmPassword))
            {
                await _userManager.ChangePasswordAsync(user, model.Password, model.NewPassword);
                await _signInManager.SignOutAsync();
                return RedirectToAction("Index");
            }
            else
            {
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<IActionResult> UploadProfilePicture()
        {
            try
            {
                var user = _userManager.GetUserAsync(User).Result;
                var file = Request.Form.Files[0];
                //var uploadFolderUrl = $"C:/profilemanagement/img";
                var uploadFolderUrl = Directory.GetCurrentDirectory() + "\\wwwroot\\img";
                string imagePath = string.Empty;
                if (file != null)
                {
                    if (!Directory.Exists(uploadFolderUrl))
                        Directory.CreateDirectory(uploadFolderUrl);
                    imagePath = Path.Combine(uploadFolderUrl, $"{user.Id}.jpg");
                    using (var fileStream = new FileStream(imagePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                    var imageByte = Helper.GetImage(imagePath);
                    user.ProfilePicture = $"/img/" + $"{user.Id}.jpg";
                    await _userManager.UpdateAsync(user);

                    return Json(imageByte);
                }
                return Json(false);
            }
            catch (Exception ex)
            {
                return Json(false);
            }

        }

        //[HttpPost]
        //public JsonResult TestEducation(string id)
        //{
        //    var sitem = id;
        //    // var user = await _userManager.GetUserAsync(User);
        //    //return Json("success");
        //    return Json(new { success = true, responseText = "Sucessfully." });
        //}

        [HttpPost]
        public async Task<IActionResult> SaveEducationModalData(string id, string institute, string Degree, string session)
        {
            //var sitem = id;
            try
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    
                    if (institute != null && Degree != null)
                    {
                        // check for duplicate value

                        var isDuplicate = CheckDuplicateEducationData(institute.ToLower(), Degree.ToLower(), user);
                        if (isDuplicate)
                        {
                            return Json(new { error = true, success = false, message = "Data already exists!." });
                        }

                        EducationInfo SingleEducationData = new EducationInfo
                        {
                            InstituteName = institute,
                            DegreeName = Degree,
                            Session = session,
                            User = user,
                            Id = Convert.ToInt32(id)
                        };
                        _educationRepository.UpdateEducation(SingleEducationData);
                        //await _educationRepository.Save();
                    }
                    // return RedirectToAction("Index", "User");
                    return Json(new { error = false, success = true, message = "data updated" });

                }
                else
                {
                    return Json(new { error = true, success = false, message = "data is not updated" });
                }
            }
            catch (Exception ex)
            {

                return Json(new { success = false, responseText = "Somthing went wrong." });
            }

            // return Json(new { success = true, responseText = "Sucessfully." });
        }

        public async Task<IActionResult> SaveExperieneModalData(string id, string company, string designation, string duration)
        {
            //var sitem = id;
            try
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    
                    if (company != null && designation != null)
                    {
                        // check for duplicate value

                        var isDuplicate = CheckDuplicateExperiencedData(designation.ToLower(), company.ToLower(), user);
                        if (isDuplicate)
                        {
                            return Json(new { error = true, success = false, message = "Data already exists!." });
                        }
                        ExperienceInfo SingleExperienceData = new ExperienceInfo
                        {
                            CompanyName = company,
                            Designation = designation,
                            Duration = duration,
                            User = user,
                            Id = Convert.ToInt32(id)
                        };
                        _experienceRepository.UpdateExperience(SingleExperienceData);
                      //  await _experienceRepository.Save();
                    }
                    //return RedirectToAction("Index", "User");
                    return Json(new { error = false, success = true, message = "data updated" });

                }
                else
                {
                    return Json(new { success = false, error=true, message = "data is not updated" });
                }
            }
            catch (Exception ex)
            {

                return Json(new { success = false, responseText = "Somthing went wrong." });
            }

            // return Json(new { success = true, responseText = "Sucessfully." });
        }



        public bool CheckDuplicateEducationData(string instituteName, string degree, ApplicationUser user)
        {
            var eduinfo = _educationRepository.GetEducationInfoForSingleUser(user.Id);
            foreach (var item in eduinfo)
            {
                if (item.InstituteName.ToLower().Trim() == instituteName.ToLower().Trim() && item.DegreeName.ToLower() == degree.ToLower().Trim())
                {
                    return true;
                }

            }
            return false;
        }

        public bool CheckDuplicateExperiencedData(string empDesignation, string companyName, ApplicationUser user)
        {
            var expInfo = _experienceRepository.GetExperienceInfoForSingleUser(user.Id);
            foreach (var item in expInfo)
            {
                if (item.Designation.ToLower().Trim() == empDesignation.ToLower().Trim() && item.CompanyName.ToLower().Trim() == companyName.ToLower().Trim())
                {
                    return true;
                }

            }
            return false;
        }

        public bool CheckUserDuplicateContactInfo(UserProfileEditViewModel profile)
        {

            var experienceInfo = _userRepository.GetAllWithOutCurrentUser(profile.UserId);

            if (experienceInfo.Any(x => x.Email == profile.Email))
            {
                return true;
            }

            else if (experienceInfo.Any(x => x.PhoneNumber == profile.PhoneNumber))
            {
                return true;
            }

            else if (experienceInfo.Any(x => x.Facebook == profile.Facebook))
            {
                return true;
            }

            else if (experienceInfo.Any(x => x.Twitter == profile.Twitter))
            {
                return true;
            }

            else
                return false;
        }
    }
}