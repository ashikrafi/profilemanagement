﻿using AutoMapper;
using ProfileManagement.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace ProfileManagement.Controllers
{
    [Authorize]
    public class _UserController : Controller
    {
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        public _UserController(IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _mapper = mapper;
            _userManager = userManager;
        }
        
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }

        // GET: Profile/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Profile/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Profile/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //// GET: Profile/UserInfo
        //public async Task<IActionResult> UserInfo()
        //{
        //    var user = await _userManager.GetUserAsync(User);
        //    ProfileViewModel profile = new ProfileViewModel();
        //    if (user != null)
        //    {
        //        profile.FullName = user.FullName ?? "";
        //        profile.LastName = user.LastName ?? "";
        //        profile.FirstName = user.FirstName ?? "";
        //        profile.Email = user.Email ?? "";
        //        profile.Mobile = user.PhoneNumber ?? "";
        //        //profile.CompanyName = user.CompanyName ?? "";
        //        //profile.OfficeAddress = user.OfficeAddress ?? "";
        //        //profile.CompanyWebsite = user.CompanyWebsite ?? "";
        //        //profile.OthersMemberEmail = user.OthersMemberEmail ?? "";
        //        //profile.ProfilePicture = user.ProfilePicture ?? "";
        //    }
        //    return View(profile);
        //}

        // POST: Profile/UserInfo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserInfo(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        // GET: Profile/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Profile/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Profile/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Profile/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
