﻿var app = {
    upperCase: new RegExp('[A-Z]'),
     lowerCase: new RegExp('[a-z]'),
     numbers: new RegExp('[0-9]'),
     specialKey: new RegExp('!@#$%^&./\\><;:"|~()+-/*'),
     currentSelection: {},
    init: function () {
        var _this = this;
        $(document).keyup(function (e) {
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                _this.closePopup();
            }
        });
       // _this.toastSetup();
        //$("#user-mobile").intlTelInput({
        //    initialCountry: "auto",
        //    geoIpLookup: function (callback) {
        //        $.get("https://ipinfo.io", function () { }, "jsonp").always(function (resp) {
        //            var countryCode = (resp && resp.country) ? resp.country : "";
        //            callback(countryCode);
        //        });
        //    },
        //    utilsScript: "../lib/intl-tel-input/js/utils.js"
        //});
     },
    uploadonDrop: function (element,e) {
        var _this = app;
        var freeImage = parseInt($("#free-image").val());
        var image = e.dataTransfer.files;
        if (image.length > 0) {
            for (var i = 0; i < image.length; i++) {
                size += image[i].size;

                filesList.push(image[i]);
            }
            var sizeinMb = ((size / 1024) / 1024).toFixed(2);
            var unit = " MB"
            if (sizeinMb < 1) {
                sizeinMb = (size / 1024).toFixed(3);
                unit = " KB";
            }
            fileCount += image.length;
          //  fileCount += $(element)[0].files.length;
            if (fileCount <= 100) {
                unitPrice = 1.19;
            } else if (fileCount > 101 && fileCount <= 500) {
                unitPrice = .99;
            }
            var freePrice = (unitPrice * freeImage).toFixed(2);
            totalPrice = (fileCount * unitPrice).toFixed(2) - freePrice;
            if (totalPrice <= 0) {
                totalPrice = 0;
            }
            $("#price-span").show();
            $(".bold-price").text("$" + totalPrice);
            var sizeinMb = ((size / 1024) / 1024).toFixed(2);
            var unit = " MB", html = "";
            if (sizeinMb < 1) {
                sizeinMb = (size / 1024).toFixed(3);
                unit = " KB";
            }
            $("#circle-image").hide();
            $("#add-files").text(fileCount + " Images selected.").addClass("margin-45percent");
            $("#size-files").text(sizeinMb + unit);
            html += '<li data-pos="">' + fileCount + ' Files ... ' + sizeinMb + unit + ' <i class="fa fa-times remove-files" style="float:right"><i></li>';
            $(document).find(".photo-upload-area").removeClass(".photo-upload-area").addClass("photo-area-padding").find('ul').empty().append(html);
            _this.uploadFiles();
        }
    },
    uploadonChange: function (element) {
        var _this = app;
        var freeImage = parseInt($("#free-image").val());
        if ($(element)[0].files.length > 0) {
            $(document).find(".pic-icon").find('i').hide();
            $(".pic-icon").append('<i class="fa fa-plus-circle photo-upload-i" aria- hidden="true" ></i > ');
            var html = '';
            var pos = '';
            for (var i = 0; i < $(element)[0].files.length; i++) {
                size += $(element)[0].files[i].size;
                pos += i + ',';
                filesList.push($(element)[0].files[i]);
            }
            var sizeinMb = ((size / 1024) / 1024).toFixed(2);
            var unit = " MB"
            if (sizeinMb < 1) {
                sizeinMb = (size / 1024).toFixed(3);
                unit = " KB";
            }
            fileCount += $(element)[0].files.length;
            if (fileCount <= 100) {
                unitPrice = 1.19;
            } else if (fileCount > 101 && fileCount <= 500) {
                unitPrice = .99;
            }
            var freePrice = (unitPrice * freeImage).toFixed(2);
            totalPrice = ((fileCount * unitPrice).toFixed(2) - freePrice).toFixed(2);
            if (totalPrice <= 0) {
                totalPrice = 0;
            }
            $("#price-span").show();
            $(".bold-price").text("$" + totalPrice);
            $("#circle-image").hide();
            $("#add-files").text(fileCount + " Images selected.").addClass("margin-45percent");
            $("#size-files").text(sizeinMb + unit);
            html += '<li data-pos="' + pos + '">' + fileCount + ' Files ... ' + sizeinMb + unit + ' <i class="fa fa-times remove-files" style="float:right"><i></li>';
            $(document).find(".photo-upload-area").removeClass(".photo-upload-area").addClass("photo-area-padding").find('ul').empty().append(html);
            _this.uploadFiles();
        } else {
            alert('please select a file.');
        }
    },
    gotoCheckout: function () {
        var _this = app;
        var data = {
            fileCount: filesList.length
        };
        $.ajax({
            type: 'POST',
            url: '/Upload/GotoCheckout',
            data: data,            
            success: function (res) {
                //$("#image-files").hide();
                //$("#uploadForm").css("width","100%").css("border","none");
                $("#checkout-div").html(res);
            }
        });
    },
    uploadFiles: function () {
        var _this = app;
        var progressEle = $("#progress");
        if (filesList.length > 0) {
            $("#upload-files-btn,#size-files,#add-files").hide();
            $("#cancel-files-btn").show();
            var fileUpload = $("#uplodFiles").get(0);
            var files = filesList;
            var fileData = new FormData();
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }
            progressEle.show().css("color", "white");
            $.ajax({
                url: '/Upload/UploadPhotosAsync',
                type: 'POST',
                contentType: false, // Not to set any content header  
                processData: false,
                data: fileData,
                processData: false,
                contentType: false,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var progress = Math.round((evt.loaded / evt.total) * 100);
                            progressEle.text(progress + "%");
                            $('.upload-circle').css({ background: "linear-gradient(to right, #08D7AD " + progress + "%,transparent " + progress + "%,transparent 100%)" });
                        }
                    }, false);
                    return xhr;
                },
                success: function (res) {
                    if (res.state == 0) {
                        $("#savedCharge").val(res.chargeId);
                        $("#upload-files-btn").show();
                        $("#cancel-files-btn").hide();
                        //$("#upload-div").hide();
                       // _this.gotoCheckout();
                    }
                }, error: function () {
                    progressEle.text("Error");
                    //$("#cancel-files-btn").hide();
                }
            });
        } else {
            alert('Please select atleast one image.')
        }
    },
    cancelupload: function () {
        var _this = app;
        $("#cancel-files-btn").hide();
        $("#upload-div,#upload-files-btn").show();
        $("#checkout-div").empty();
        $("#progress").hide().text("");
        $("#uplodFiles").val("");
        $("#circle-image").show();
        $(".upload-circle").css("background", "none");
        $("#add-files").show().text("No files added yet.").removeClass("margin-45percent");
         _this.size = 0;
        _this.fileCount = 0;
         _this.filesList = [];
    },
    checkoutNow: function () {
        var _this = app;
        var data = {
            chargeId: $("#savedCharge").val(),
            comment: $("#comment").val()
        };
        if (data.comment == "") {
            $("#comment").addClass("red-border");
            return;
        } else {
            $("#comment").removeClass("red-border");
        }
        $.ajax({
            url: '/Upload/UploadConfirmation',
            data: data,
            type: 'POST',
            success: function (res) {           
                $("#success-upload").show();               
                $("#full-upload,#upload-actions").hide();
            }, error: function () {              
            }
        });
    },
    signUp: function () {
        var _this = app;
        var data = {
            firstName: $("#FirstName").val(),
            lastName: $("#LastName").val(),
            regEmail: $("#RegEmail").val(),
            regPassword: $("#RegPassword").val(),
            ConfirmPassword: $("#ConfirmPassword").val()
        };
        $("#register-form").find(".form-control").each(function () {
            if ($(this).val() == "") {
                $(this).addClass("red-border");
            } else {
                $(this).removeClass("red-border");
            }
        });
        if (!data.regPassword.match(app.upperCase) || !data.ConfirmPassword.match(app.upperCase)) {
            if (!data.regPassword.match(app.upperCase)) {
                $("#RegPassword").addClass("red-border");
            } if (!data.ConfirmPassword.match(app.upperCase)) {
                $("#ConfirmPassword").addClass("red-border");
            }
            $("#register-error").show().text("Password length must be 9 containing atleast one number,uppercase and lowercase letter.");
            return;
        }
        if (!data.regPassword.match(app.lowerCase) || !data.ConfirmPassword.match(app.lowerCase)) {
            if (!data.regPassword.match(app.lowerCase)) {
                $("#RegPassword").addClass("red-border");
            } if (!data.ConfirmPassword.match(app.lowerCase)) {
                $("#ConfirmPassword").addClass("red-border");
            }
            $("#register-error").show().text("Password length must be 9 containing atleast one number,uppercase and lowercase letter.");
            return;
        }
        if (!app.validatePassword(data.regPassword) || !app.validatePassword(data.ConfirmPassword)) {
            if (!app.validatePassword(data.regPassword)) {
                $("#RegPassword").addClass("red-border");
            } if (!app.validatePassword(data.ConfirmPassword)) {
                $("#ConfirmPassword").addClass("red-border");
            }
            $("#register-error").show().text("Password length must be 9 containing atleast one number,uppercase and lowercase letter.");
            return;
        }
        if (data.ConfirmPassword.length < 9 || data.regPassword.length<9) {
            if (data.ConfirmPassword.length < 9) {
                $("#RegPassword").addClass("red-border");
            } if (data.regPassword.length < 9) {
                $("#ConfirmPassword").addClass("red-border");
            }
            $("#register-error").show().text("Password length must be 9 containing atleast one number,uppercase and lowercase letter.");
            return;
        }
        else {
            $("#ConfirmPassword").removeClass("red-border");
            $("#RegPassword").removeClass("red-border");
        }
        $("#reg-error,#register-error").hide();        
        if (!validateEmail(data.regEmail)) {
            $("#RegEmail").addClass("red-border");
        }
        if (data.ConfirmPassword != data.regPassword) {
            $("#register-error").show().find('p').text("Password mismatch.");
            $("#ConfirmPassword").addClass("red-border");
            $("#RegPassword").addClass("red-border");
            return;
        }
        if ($("#register-form").find(".red-border").length <= 0) {
            $.ajax({
                url: '/Account/Register',
                type: 'POST',
                data: data,
                success: function (res) {
                    if (res.status) {
                        $("#register-form,#reg-error,#myTab,#thitd-party-logins,#or-text").hide();
                        $("#reg-success").show().find('p').text(res.msg);
                    }
                    else {
                        $("#register-form,#reg-success,#myTab,#thitd-party-logins,#or-text").hide();
                        $("#reg-error").show().find('p').text(res.msg);
                    }
                }
            });
        } else {
            return;
        }
    },
    searchOderById: function (element) {
        _this = app;
        var data = { orderId: $(element).val() };
        $.ajax({
            type: 'GET',
            url:'/Order/_AllOrders',
                data:data,
                success: function (res) {
                    $("#wrap_order").empty().html(res);
                }    
        });
    },
    sortByOrder: function (element) {
        var data = { sortByOrder: !$(element).data("value") };
        if ($(element).find('i').hasClass("fa-caret-up")) {
            $(element).find('i').removeClass("fa-caret-up").addClass("fa-caret-down");
        } else {
            $(element).find('i').removeClass("fa-caret-down").addClass("fa-caret-up");
        }
        var sortValue = data.sortByOrder;
        $(element).data("value", sortValue);
        $.ajax({
            type: 'GET',
            url: '/Order/_AllOrders',
            data: data,
            success: function (res) {
                $("#wrap_order").empty().html(res);
            }
        });
    },
    sortByDate: function (element) {
        var data = { sortBydate: !$(element).data("value") };
        if ($(element).find('i').hasClass("fa-caret-up")) {
            $(element).find('i').removeClass("fa-caret-up").addClass("fa-caret-down");
        } else {
            $(element).find('i').removeClass("fa-caret-down").addClass("fa-caret-up");
        }
        var sortValue = data.sortBydate;
        $(element).data("value", sortValue);
        $.ajax({
            type: 'GET',
            url: '/Order/_AllOrders',
            data: data,
            success: function (res) {
                $("#wrap_order").empty().html(res);
            }
        });
    },
    appsearchByDate: function (element) {
        _this = app;
        var data = { orderDate: $(element).val() };

            $.ajax({
                type: 'GET',
                url: '/Order/_AllOrders',
                data: data,
                success: function (res) {
                    $("#wrap_order").empty().html(res);
                }
            });
    },
    searchByStatus: function (element) {
        _this = app;
        var data = { status: $(element).val() };
        $.ajax({
            type: 'GET',
            url: '/Order/_AllOrders',
            data: data,
            success: function (res) {
                $("#wrap_order").empty().html(res);
            }
        });
    },
    searchByPayment: function (element) {
        _this = app;
        var data = { paymentStatus: $(element).val() };
        $.ajax({
            type: 'GET',
            url: '/Order/_AllOrders',
            data: data,
            success: function (res) {
                $("#wrap_order").empty().html(res);
            }
        });
    },
    cancelOrder: function () {
        $("#upload-div").show();
        $("#checkout-div").empty();
        $("#progress").text("");
        $("#uplodFiles").val("");
        $("#circle-image").show();
        $("#add-files").show().text("No files added yet.");
        $(".upload-circle").css("background", "none");
         size = 0;
        fileCount = 0;
         filesList = [];
    },
    downloadZip: function (chargeId) {
        var _this = app;
        var data = {
            chargeId:chargeId
        };
        location.href = "/Completion/DownloadZipFile?chargeId=" + chargeId;
    },
    exploreCompletedImages: function (chargeId) {
        var _this = app;
        var data = {
            chargeId:chargeId
        };
        $.ajax({
            type: 'POST',
            url: '/Completion/ExploreProcessedCharge',
            data: data,
            success: function (res) {
                $("#image-area").hide();
                $("#explore-area").html(res);
                $("#loadingDiv").hide();
            }
        });
    },
    backtoCompletion: function () {
        var _this = app;
        $("#image-area").show();
        $("#explore-area").empty();
    },
    saveProfile: function () {
        var _this = app;
        var isValid = $("#user-mobile").intlTelInput("isValidNumber");
        if ($("#user-mobile").val() == "" || !isValid) {
            $("#user-mobile").addClass("red-border");
            return;
        } else {
            $("#user-mobile").removeClass("red-border");
        }
        var url = '/Profile/SaveProfile';     
        var profile = {                                
            Mobile: $('#user-mobile').intlTelInput("getNumber"),
            CompanyName: $('#company-name').val(),
            OfficeAddress: $('#offcie-address').val(),
            CompanyWebsite: $('#company-website').val(),
            OthersMemberEmail: $('#other-email').val()
        };
            $("#error-msg").hide();
            $.ajax({
                type: "POST",
                url: url,
                data: { profile: profile },
                success: function (data) {
                    if (data === 'success') {
                       // $("#circle").hide();
                        $("#error-msg").show().css("color", "green");
                        $("#error-msg").text("Successfully Updated.");
                        setTimeout(function () {
                            location.href = "/";
                        }, 2000)
                    }else if (data == 'pass') {
                        $("#circle").hide();
                        $("#error-msg").show().css("color", "green").text("Successfully Updated.")
                        setTimeout(function () {
                            window.location.href = "/Account/Login";
                        }, 4000);

                    }else if (data == 'error') {
                        // $("#circle").hide();
                    }
                    else {
                        $("#error-msg").show().css("color", "red");
                        $("#error-msg").text(data);
                    }
                }, error: function () {
                   // $("#circle").hide();
                }
            });    
    },
    saveEducationInfo: function () {
        var _this = app;
        //var isValid = $("#user-mobile").intlTelInput("isValidNumber");
        //if ($("#user-mobile").val() == "" || !isValid) {
        //    $("#user-mobile").addClass("red-border");
        //    return;
        //} else {
        //    $("#user-mobile").removeClass("red-border");
        //}
        var url = '/User/SaveEducationInfo';
        var profile = {
            InstituteName: $('#institute-name').val(),
            DegreeName: $('#degree-name').val(),
            Session: $('#session').val()
        };
        $("#error-msg").hide();
        $.ajax({
            type: "POST",
            url: url,
            data: { profile: profile },
            success: function (data) {
                if (data === 'success') {
                    // $("#circle").hide();
                    $("#error-msg").show().css("color", "green");
                    $("#error-msg").text("Successfully Updated.");

                    //setTimeout(function () {
                    //    location.href = "/";
                    //}, 2000)
                    //$("ul.tabs li").removeClass("active");
                    $("#education-tab").addClass("active").show();
                }
                //else if (data == 'pass') {
                //    $("#circle").hide();
                //    $("#error-msg").show().css("color", "green").text("Successfully Updated.")
                //    //setTimeout(function () {
                //    //    window.location.href = "/Account/Login";
                //    //}, 4000);

                //} else if (data == 'error') {
                //    // $("#circle").hide();
                //}
                else {
                    $("#error-msg").show().css("color", "red");
                    $("#error-msg").text(data);
                }
            }, error: function () {
                // $("#circle").hide();
            }
        });
    },

    updatePassword: function () {        
        var url = '/Profile/updatePassword';  
        var update = {
            NewPassword: $("#newPassword").val(),
            Password: $("#password").val(),
            ConfirmPassword: $("#confirmPassword").val(),
        };
        if (update.Password != "") {
            $("#password").removeClass("red-border");
            if (update.NewPassword == update.ConfirmPassword) {
                $("#circle").show();
                $('.second.circle').circleProgress({
                    value: 1, fill: { color: '#08D7AD' }
                }).on('circle-animation-progress', function (event, progress) {
                    $(this).find('strong').html(Math.round(100 * progress) + '<i>%</i>');
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: update,
                    success: function (data) {
                        $("#circle").hide();
                        if (data) {
                            $("#error-msg").show().css("color", "green").text("Successfully Updated.")
                            setTimeout(function () {
                                window.location.href = "/Account/Login";
                            }, 4000);
                        } else {

                            $("#error-msg").show().css("color", "red").text("Update failed.")
                        }
                    }, error: function () {
                        $("#loadingDiv").hide();
                    }
                });
            } else {
                $("#confirmPassword,#newPassword").addClass("red-border");
                return;
            }
        } else {
            $("#password").addClass("red-border");
            return;
        }        
    },
    hideProfile: function () {
        var _this = app;
        $("#profile-pass,#passUpdate").addClass("animated").addClass("slideInUp").toggle();
        $("#profile-info,#profile-update").toggle();
    },
    validateEmail:function(Email) {
        var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
return $.trim(Email).match(pattern) ? true : false;
    },
    validatePassword: function (pass) {
        var pattern = /[^\w\s]/gi;
        return $.trim(pass).match(pattern) ? true : false;
    },
    toastSetup: function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "5000",
            // "hideDuration": "2000",
            // "timeOut": "5000",
            "extendedTimeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    },
    login: function (element) {
        var _this = app;
        //if ($("#login-email").val() == "" || !_this.validateEmail($("#login-email").val()) || $("#login-password").val() == "") {
        //    //if ($("#login-email").val() == "" || !_this.validateEmail($("#login-email").val())) {
        //    if ($("#login-email").val() == "") {
        //        $("#login-email").addClass("red-border");
        //    } else {
        //        $("#login-email").removeClass("red-border");
        //    }
        //    if ($("#login-password").val() == "") {
        //        $("#login-password").addClass("red-border");
        //    }
        //    else {
        //        $("#login-password").removeClass("red-border");
        //    }
        //    return;
        //}
        var data = {
            email: $("#login-email").val(),
            password: $("#login-password").val()
        };
        $.ajax({
            url: '/Account/Login',
            method: 'POST',
            data: data,
            success: function (res) {
                if (res.status == true) {
                    location.href = res.message;
                }
                if (res.status == false) {
                    toastr["error"](res.message);
                }
            }
        });
    },
    showLogin: function (element) {
        $(element).hide();
        $("#sign-in-form,#myTab").show();
        $("#forgot-area").hide().find('p').remove();
        $("#modal-cross").hide();
    },
    logout: function (element) {
        var _this = app;
        
        $.ajax({
            url: '/Account/Logout',
            method: 'POST',
            //data: data,
            success: function (res) {
                if (res.status == true) {
                    location.href = res.message;
                }
                if (res.status == false) {
                    toastr["error"](res.message);
                }
            }
        });
    },
    viewImage: function (element) {
        var _this = app;
        _this.currentSelection = $(element);
        $(element).addClass("current");
        var imageSrc = $(element).attr("src");
        var width = (60 * $(window).width()) / 100;
        var height = (80 * $(window).height()) / 100;
        $(".image-popup").height($(window).height());
        $('.image-popup').empty().show().append('<div class="row"><span style="margin-right:5%;float:right;font-size:20px;color:white;cursor:pointer" onclick="javascript:app.closePopup(this)">X</span></div>' +
            '<i style="float:left;margin-left:5%;color:white;font-size:30px;cursor:pointer;margin-top:'+$(window).height()/3+'px" class="fa fa-chevron-left" onclick="javascript:app.nextPrev(this,1)"></i>' +
            '<i style="float:right;margin-right:5%;color:white;font-size:30px;cursor:pointer;margin-top:' + $(window).height() / 3 +'px" class="fa fa-chevron-right" onclick="javascript:app.nextPrev(this,2)"></i>' +
            '<img src= "' + imageSrc + '" style= "margin:0 auto;max-width:' + width + 'px!important;max-height:' + height + 'px!important;margin:2%;margin-top:-20px"/>');
    },

    closePopup: function (element) {
        var _this = element;
        $(".image-popup").empty().hide();
        $('img').removeClass("current");
    },
    nextPrev: function (element, action) {
        var _this = element;
        var src;
        var allImage = $("#explore-area").find('img');
        for (var i = 0; i < allImage.length; i++) {
            if (allImage[i].getAttribute("class")=="current") {
                allImage[i].setAttribute("class","")
                if (action == 1) {
                    if (i == 0) {
                        allImage[allImage.length - 1].setAttribute("class", "current");
                        src = allImage[allImage.length - 1].getAttribute("src");
                    }
                    else {
                        allImage[i - 1].setAttribute("class", "current");
                        src = allImage[i - 1].getAttribute("src");
                    }
                }
                else if (action == 2) {
                    if (i == allImage.length-1) {
                        allImage[0].setAttribute("class", "current");
                        src = allImage[0].getAttribute("src");
                    }
                    else {
                      allImage[i + 1].setAttribute("class", "current");
                        src = allImage[i + 1].getAttribute("src");
                    }
                }
                break;
            }
        }
        $(".image-popup").find('img').attr("src", src);
    }
};
$(document).ready(function () {
    app.init();
});
//$(document).ajaxStart(function () {
//    $("#fakeLoader").fakeLoader({
//        timeToHide: 120000, //Time in milliseconds for fakeLoader disappear
//        zIndex: 999, // Default zIndex
//        spinner: "spinner3",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7' 
//        bgColor: "#323534", //Hex, RGB or RGBA colors            
//    });
//}).ajaxStop(function () {
//    $("#fakeLoader").remove();
//    $('body').append('<div id="fakeLoader"></div>')
//    });
